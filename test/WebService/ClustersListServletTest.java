/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package WebService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Sabrina
 */
public class ClustersListServletTest {

    public ClustersListServletTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doGet method, of class ClustersListServlet.
     */
    @Test
    public void testDoGet() throws Exception {
        System.out.println("doGet");
        String response;
        BufferedWriter wrt = new BufferedWriter(new FileWriter(new File(TestConfig.dir + "clustersList.out")));
        response = Connection.connect("Get", "/clusters", "");
        wrt.write("test 1");
        wrt.newLine();
        wrt.write(response, 0, response.length());
        wrt.close();
    }
}