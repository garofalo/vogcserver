/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package WebService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Sabrina
 */
public class DeleteAttributeValueServletTest {

    public DeleteAttributeValueServletTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doGet method, of class DeleteAttributeValueServlet.
     */
    @Test
    public void testDoGet() throws Exception {
        BufferedReader r = new BufferedReader(new FileReader(new File(TestConfig.dir + "deleteValue.in")));
        BufferedWriter wrt = new BufferedWriter(new FileWriter(new File(TestConfig.dir + "deleteValue.out")));
        String userId;
        String objectId;
        String vobjHasAttId;
        String response;
        int i = 0;

        while ((userId = r.readLine()) != null) {
            i++;
            objectId = r.readLine();
            objectId = objectId.replace(' ', '+');
            vobjHasAttId = r.readLine();
            response = Connection.connect("Get", "/deleteValue", "?userId=" + userId + "&objectId=" + objectId + "&vobjHasAttId=" + vobjHasAttId);
            wrt.write("test " + i);
            wrt.newLine();
            wrt.write(response, 0, response.length());
            wrt.newLine();
        }
        wrt.close();

    }
}