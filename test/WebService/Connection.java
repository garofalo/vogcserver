/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package WebService;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

/**
 *
 * @author sabrina checola
 */
public class Connection {

    static String ip = "http://localhost:8085/SERVER/";

    public static String connect(String method, String servlet, String query) {
        HttpClient client = new HttpClient();
        HttpMethod request = null;
        if (method.equals("Get")) {
            request = new GetMethod(ip + "/" + servlet + query);
        } else if (method.equals("Put")) {
            request = new PutMethod(ip + "/" + servlet + query);
        } else if (method.equals("Post")) {
            request = new PostMethod(ip + "/" + servlet + query);
        } else if (method.equals("Delete")) {
            request = new DeleteMethod(ip + "/" + servlet + query);
        }
        try {
            client.executeMethod(request);
            return request.getResponseBodyAsString();
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";


    }

    public static String upload(String servlet, String path, String query) {
        try {
            HttpClient c = new HttpClient();
            PostMethod method = new PostMethod(ip + "/" + servlet + query);

            File f = new File(path);
            Part[] parts = {new FilePart(f.getName(), f)};
            MultipartRequestEntity request = new MultipartRequestEntity(parts, method.getParams());
            method.setRequestEntity(request);
            request.getContentType();
            c.executeMethod(method);
            return method.getResponseBodyAsString();
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);



        }
        return "";
    }
}
