/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package WebService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Sabrina
 */
public class AuthenticateServletTest {

    public AuthenticateServletTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doGet method, of class AuthenticateServlet.
     */
    @Test
    public void testDoGet() throws Exception {
        System.out.println("doGet");
        BufferedReader r = new BufferedReader(new FileReader(new File(TestConfig.dir + "authenticate.in")));
        BufferedWriter wrt = new BufferedWriter(new FileWriter(new File(TestConfig.dir + "authenticate.out")));
        String userId;
        String response;
        int i = 0;

        while ((userId = r.readLine()) != null) {
            i++;
            response = Connection.connect("Get", "/authenticate", "?userId=" + userId);
            wrt.write("test " + i);
            wrt.newLine();
            wrt.write(response, 0, response.length());
            wrt.newLine();
        }
        wrt.close();
    }
}