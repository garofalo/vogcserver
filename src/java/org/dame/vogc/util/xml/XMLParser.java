package org.dame.vogc.util.xml;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.dame.vogc.datatypes.Author;
import org.dame.vogc.datatypes.BiblioNotes;
import org.dame.vogc.datatypes.BiblioRef;
import org.dame.vogc.datatypes.GcAttribute;
import org.dame.vogc.datatypes.Gcluster;
import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.datatypes.Image;
import org.dame.vogc.datatypes.Notes;
import org.dame.vogc.datatypes.Paper;
import org.dame.vogc.datatypes.PlsAttribute;
import org.dame.vogc.datatypes.Pulsar;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.datatypes.Star;
import org.dame.vogc.datatypes.StarAttribute;
import org.dame.vogc.datatypes.StarHasAttribute;
import org.dame.vogc.datatypes.VObject;
import org.dame.vogc.error.Error;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;
import org.jdom.Document;
import org.jdom.Element;

/**
 *
 * @author Luca
 */
public class XMLParser {

    private final IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
    private XMLGenerator xgen;
    public String stringTmp;

    public void parseGclusterAttribute(Document doc, PrintWriter out) throws Exception {

        String sourceId = null, attName = null;

        int isNew = 0, newId = 0, k;
        xgen = new XMLGenerator(out);
        VObject obj = new VObject(1);
        Gcluster cluster = new Gcluster();
        GclusterHasAttribute gcAtt = null;
        GcAttribute attribute = null;
        //     Namespace ns = Namespace.getNamespace("xsi", "http://www.ivoa.net/xml/VOTable/v1.1");

        Element rootVOTABLE = doc.getRootElement();
        List childrenPARAM = rootVOTABLE.getChildren("PARAM");
        Iterator iterator = childrenPARAM.iterator();
        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();

            if (element.getAttributeValue("name").equals("Id")) {
                obj.setId(element.getAttributeValue("value"));
                cluster.setClusterId(element.getAttributeValue("value"));
                stringTmp = element.getAttributeValue("value");
                cluster.setClusterType(Integer.parseInt(element.getAttributeValue("utype")));
            }
            if (element.getAttributeValue("name").equals("Name")) {
                cluster.setName(element.getAttributeValue("value"));
            }
            if (element.getAttributeValue("name").equals("sourceId")) {
                sourceId = element.getAttributeValue("value");

            }
        }
        vogc.insertVObject(obj);   // -- ELIMINA COMMENTO
        vogc.insertGcluster(cluster); // -- ELIMINA COMMENTO

        Element RESOURCE = rootVOTABLE.getChild("RESOURCE");

        List childrenRESOURCE = RESOURCE.getChildren("TABLE");
        Iterator tableList = childrenRESOURCE.iterator();

        while (tableList.hasNext()) {
            Element tableElement = (Element) tableList.next();
            // TABELLA ATTRIBUTI
            if (tableElement.getAttributeValue("ID").equals("gcAtt")) {
                Element tableDataElement = tableElement.getChild("DATA").getChild("TABLEDATA");
                List childrenTable = tableDataElement.getChildren("TR");
                Iterator trList = childrenTable.iterator();
                while (trList.hasNext()) {
                    gcAtt = new GclusterHasAttribute();
                    gcAtt.setSourceId(sourceId);
                    gcAtt.setGclusterId(obj.getId());
                    gcAtt.setDate(null); // viene inserito dal DB
                    gcAtt.setId(0);      // viene inserito dal DB
                    Element elementTR = (Element) trList.next();
                    List childrenTR = elementTR.getChildren("TD");
                    Iterator tdList = childrenTR.iterator();
                    k = 0;
                    gcAtt.setFirst(false);
                    while (tdList.hasNext()) {
                        Element elementTD = (Element) tdList.next();
                        if (k == 0) {
                            isNew = Integer.parseInt(elementTD.getTextTrim());
                            k++;
                        } else if (k == 1) {
                            gcAtt.setValue(elementTD.getTextTrim());
                            k++;
                        } else if (k == 2) {
                            attName = elementTD.getTextTrim();
                            k++;
                        } else if (k == 3 && isNew == 1) {
                            try {
                                vogc.checkGcAttributeIsNew(attName);
                            } catch (VogcException ex) {
                                xgen.generateErrorReport(new Error(ex.getMessage(), ex.getCode()), attName);
                            }
                            //vogc.checkGcAttributeIsNew(attName); // controllo che il nome non sia già presente nel DB
                            gcAtt.setFirst(true);
                            attribute = new GcAttribute();
                            attribute.setId(0);
                            attribute.setName(attName);
                            attribute.setDescription(elementTD.getTextTrim());
                            k++;
                        } else if (k == 4) {
                            attribute.setUcd(elementTD.getTextTrim());
                            k++;
                        } else if (k == 5) {
                            attribute.setDatatype(elementTD.getTextTrim());
                            k++;
                        } else if (k == 6) {
                            attribute.setType(Integer.parseInt(elementTD.getTextTrim()));
                            k++;
                        }

                    }
                    if (isNew == 1) {
                        attribute.setPrimaryAtt(false);
                        newId = vogc.insertGcAttribute(attribute);
                        gcAtt.setGcAttId(newId);
                    } else {
                        try {

                            newId = vogc.selectGcAttributeIdByName(attName);
                        } catch (VogcException ex) {
                            xgen.generateErrorReport(new Error(ex.getMessage(), ex.getCode()), attName);
                        }
                        gcAtt.setGcAttId(newId);
                    }
                    //   System.out.println("gcAtt ="+gcAtt.getGcAttId());
                    vogc.insertGclusterHasAttribute(gcAtt);
                }
            }
        }
    }

    public void parsePulsarAttribute(Document doc, PrintWriter out) throws Exception {

        String sourceId = null, attName = null;
        int isNew = 0, newId = 0;
        xgen = new XMLGenerator(out);
        VObject obj = new VObject(2);
        Pulsar pulsar = new Pulsar();
        PulsarHasAttribute plsAtt = null;
        PlsAttribute attribute = null;

        Element rootVOTABLE = doc.getRootElement();
        List childrenPARAM = rootVOTABLE.getChildren("PARAM");
        Iterator iterator = childrenPARAM.iterator();
        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();

            if (element.getAttributeValue("name").equals("Id")) {
                obj.setId(element.getAttributeValue("value"));
                pulsar.setId(element.getAttributeValue("value"));
            }
            if (element.getAttributeValue("name").equals("gclusterId")) {   // ammasso a cui appartiene la pulsar
                pulsar.setGclusterId(element.getAttributeValue("value"));
            }

            if (element.getAttributeValue("name").equals("sourceId")) {
                sourceId = element.getAttributeValue("value");
            }
        }

        vogc.insertVObject(obj);
        if (stringTmp != null) {
            Gcluster tmp = new Gcluster();
            tmp.setClusterId(pulsar.getGclusterId());
            tmp.setClusterType(1);
            //tmp.setName());

            vogc.insertGcluster(tmp);
        }
        vogc.insertPulsar(pulsar);  //-- ELIMINA COMMENTO

        Element RESOURCE = rootVOTABLE.getChild("RESOURCE");

        List childrenRESOURCE = RESOURCE.getChildren("TABLE");
        Iterator tableList = childrenRESOURCE.iterator();

        while (tableList.hasNext()) {
            Element tableElement = (Element) tableList.next();
            // TABELLA ATTRIBUTI
            if (tableElement.getAttributeValue("ID").equals("plsAtt")) {
                Element tableDataElement = tableElement.getChild("DATA").getChild("TABLEDATA");
                List childrenTable = tableDataElement.getChildren("TR");
                Iterator trList = childrenTable.iterator();
                while (trList.hasNext()) {
                    plsAtt = new PulsarHasAttribute();
                    plsAtt.setSourceId(sourceId);
                    plsAtt.setPlsId(obj.getId());
                    plsAtt.setDate(null); // viene inserito dal DB
                    plsAtt.setId(0);      // viene inserito dal DB
                    Element elementTR = (Element) trList.next();
                    List childrenTR = elementTR.getChildren("TD");
                    Iterator tdList = childrenTR.iterator();
                    int k = 0;
                    plsAtt.setFirst(false);
                    while (tdList.hasNext()) {
                        Element elementTD = (Element) tdList.next();
                        if (k == 0) {
                            isNew = Integer.parseInt(elementTD.getTextTrim());
                            k++;
                        } else if (k == 1) {
                            plsAtt.setValue(elementTD.getTextTrim());
                            k++;
                        } else if (k == 2) {
                            attName = elementTD.getTextTrim();
                            k++;
                        } else if (k == 3 && isNew == 1) {
                            try {
                                vogc.checkPlsAttributeIsNew(attName);
                            } catch (VogcException ex) {
                                xgen.generateErrorReport(new Error(ex.getMessage(), ex.getCode()), attName);
                            }
                            plsAtt.setFirst(true);
                            attribute = new PlsAttribute();
                            attribute.setId(0);
                            attribute.setName(attName);
                            attribute.setDescription(elementTD.getTextTrim());
                            k++;
                        } else if (k == 4) {
                            attribute.setUcd(elementTD.getTextTrim());
                            k++;
                        } else if (k == 5) {
                            attribute.setDatatype(elementTD.getTextTrim());
                            k++;
                        }

                    }
                    if (isNew == 1) {
                        attribute.setPrimaryAtt(false);
                        newId = vogc.insertPlsAttribute(attribute);
                        plsAtt.setPlsAttId(newId);
                    } else {
                        try {
                            newId = vogc.selectPlsAttributeIdByName(attName);
                        } catch (VogcException ex) {
                            xgen.generateErrorReport(new Error(ex.getMessage(), ex.getCode()), attName);
                        }
                        plsAtt.setPlsAttId(newId);
                    }
                    vogc.insertPulsarHasAttribute(plsAtt);

                }
            }
        }
    }

    public void parseStarAttribute(Document doc, PrintWriter out) throws Exception {

        String sourceId = null, attName = null;
        int isNew = 0, newId = 0;
        xgen = new XMLGenerator(out);
        VObject obj = new VObject(3);
        Star star = new Star();
        StarHasAttribute starAtt = null;
        StarAttribute attribute = null;

        Element rootVOTABLE = doc.getRootElement();
        List childrenPARAM = rootVOTABLE.getChildren("PARAM");
        Iterator iterator = childrenPARAM.iterator();
        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();

            if (element.getAttributeValue("name").equals("Id")) {
                obj.setId(element.getAttributeValue("value"));
                star.setId(element.getAttributeValue("value"));
            }
            if (element.getAttributeValue("name").equals("gclusterId")) {
                star.setGclusterId(element.getAttributeValue("value"));
            }
            if (element.getAttributeValue("name").equals("sourceId")) {
                sourceId = element.getAttributeValue("value");
            }
        }
        vogc.insertVObject(obj);    //-- ELIMINA COMMENTO
        vogc.insertStar(star);  //-- ELIMINA COMMENTO

        Element resource = rootVOTABLE.getChild("RESOURCE");

        List childrenRESOURCE = resource.getChildren("TABLE");
        Iterator tableList = childrenRESOURCE.iterator();

        while (tableList.hasNext()) {
            Element tableElement = (Element) tableList.next();
            // TABELLA ATTRIBUTI
            if (tableElement.getAttributeValue("ID").equals("starAtt")) {
                Element tableDataElement = tableElement.getChild("DATA").getChild("TABLEDATA");
                List childrenTable = tableDataElement.getChildren("TR");
                Iterator trList = childrenTable.iterator();
                while (trList.hasNext()) {
                    starAtt = new StarHasAttribute();
                    starAtt.setSourceId(sourceId);
                    starAtt.setStarId(obj.getId());
                    starAtt.setDate(null); // viene inserito dal DB
                    starAtt.setId(0);      // viene inserito dal DB
                    Element elementTR = (Element) trList.next();
                    List childrenTR = elementTR.getChildren("TD");
                    Iterator tdList = childrenTR.iterator();
                    int k = 0;
                    starAtt.setFirst(false);
                    while (tdList.hasNext()) {
                        Element elementTD = (Element) tdList.next();
                        if (k == 0) {
                            isNew = Integer.parseInt(elementTD.getTextTrim());
                            k++;
                        } else if (k == 1) {
                            starAtt.setValue(elementTD.getTextTrim());
                            k++;
                        } else if (k == 2) {
                            attName = elementTD.getTextTrim();
                            k++;
                        } else if (k == 3 && isNew == 1) {
                            try {
                                vogc.checkStarAttributeIsNew(attName);
                            } catch (VogcException ex) {
                                xgen.generateErrorReport(new Error(ex.getMessage(), ex.getCode()), attName);
                            }
                            starAtt.setFirst(true);
                            attribute = new StarAttribute();
                            attribute.setId(0);
                            attribute.setName(attName);
                            attribute.setDescription(elementTD.getTextTrim());
                            k++;
                        } else if (k == 4) {
                            attribute.setUcd(elementTD.getTextTrim());
                            k++;
                        } else if (k == 5) {
                            attribute.setDatatype(elementTD.getTextTrim());
                            k++;
                        }

                    }
                    if (isNew == 1) {
                        attribute.setPrimaryAtt(false);
                        newId = vogc.insertStarAttribute(attribute);
                        starAtt.setStarAttId(newId);
                    } else {
                        try {
                            newId = vogc.selectStarAttributeIdByName(attName);
                        } catch (VogcException ex) {
                            xgen.generateErrorReport(new Error(ex.getMessage(), ex.getCode()), attName);
                        }
                        starAtt.setStarAttId(newId);
                    }
                    vogc.insertStarHasAttribute(starAtt);

                }
            }
        }
    }

    public void parseNote(Document doc, PrintWriter out) throws Exception {

        int type, newId;
        String objId, userId = null;
        BiblioNotes bn = null;
        Notes note = new Notes(0, null, 0, 0, 0, null, 0, null, null, null, 0);

        Element rootVOTable = doc.getRootElement();
        List childrenPARAM = rootVOTable.getChildren("PARAM");
        Iterator iterator = childrenPARAM.iterator();

        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();
            if (element.getAttributeValue("name").equals("objectId")) {
                objId = element.getAttributeValue("value");
                type = Integer.parseInt(element.getAttributeValue("utype"));
                switch (type) {
                    case 1:
                        bn = new BiblioNotes(0, objId, 0, 0, 0, null, 0, null, 2);
                        break;

                    case 2:
                        bn = new BiblioNotes(0, "0000000000", Integer.parseInt(objId), 0, 0, userId, 0, null, 1);
                        break;
                    case 3:
                        bn = new BiblioNotes(0, "0000000000", 0, Integer.parseInt(objId), 0, userId, 0, null, 1);
                        break;
                    case 4:
                        bn = new BiblioNotes(0, "0000000000", 0, 0, Integer.parseInt(objId), userId, 0, null, 1);
                        break;
                    case 5:
                        bn = new BiblioNotes(0, "0000000000", 0, 0, 0, userId, Integer.parseInt(objId), null, 1);
                        break;

                }

            }
            if (element.getAttributeValue("name").equals("userId")) {
                bn.setUserId(element.getAttributeValue("value"));
            }
        }
        Element resource = rootVOTable.getChild("RESOURCE");
        Element tableElement = resource.getChild("TABLE");
        List childrenTable = tableElement.getChildren("PARAM");
        Iterator paramList = childrenTable.iterator();
        while (paramList.hasNext()) {  // figli di TABLE notes
            Element paramElement = (Element) paramList.next();
            if (paramElement.getAttributeValue("name").equals("title")) {
                note.setTitle(paramElement.getAttributeValue("value"));
                if (paramElement.getChild("DESCRIPTION").getTextTrim().length() > 0) {
                    note.setDescription(paramElement.getChild("DESCRIPTION").getText());
                }
            }
            if (paramElement.getAttributeValue("name").equals("type")) {
                note.setNoteType(Integer.parseInt(paramElement.getAttributeValue("value")));
            }
        }
        newId = vogc.insertBiblioNotes(bn);
        note.setId(newId);
        vogc.insertNotes(note);

    }

    public void parseBiblioRef(Document doc, PrintWriter out) throws Exception {
        String objId = null, userId = null;
        int type, newPaper, k, newAuthor, newId;
        BiblioNotes bn = null;
        BiblioRef bibRef = new BiblioRef(0, objId, 0, 0, 0, null, 0, 0, null, null, null, null, null);
        Paper paper = new Paper(0, null, null);
        Author author;
        List<Author> authorList = new ArrayList<>(5);

        Element rootVOTable = doc.getRootElement();
        List childrenParams = rootVOTable.getChildren("PARAM");
        Iterator iterator = childrenParams.iterator();

        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();
            if (element.getAttributeValue("name").equals("objectId")) {
                objId = element.getAttributeValue("value");
                type = Integer.parseInt(element.getAttributeValue("utype"));
                switch (type) {
                    case 1:
                        bn = new BiblioNotes(0, objId, 0, 0, 0, null, 0, null, 2);
                        break;

                    case 2:
                        bn = new BiblioNotes(0, "0000000000", Integer.parseInt(objId), 0, 0, userId, 0, null, 2);
                        break;
                    case 3:
                        bn = new BiblioNotes(0, "0000000000", 0, Integer.parseInt(objId), 0, userId, 0, null, 2);
                        break;
                    case 4:
                        bn = new BiblioNotes(0, "0000000000", 0, 0, Integer.parseInt(objId), userId, 0, null, 2);
                        break;
                    case 5:
                        bn = new BiblioNotes(0, "0000000000", 0, 0, 0, userId, Integer.parseInt(objId), null, 2);
                        break;

                }

            }
            if (element.getAttributeValue("name").equals("userId")) {
                bn.setUserId(element.getAttributeValue("value"));
            }
        }

        Element resource = rootVOTable.getChild("RESOURCE");
        List childrenResource = resource.getChildren("TABLE");
        Iterator tableList = childrenResource.iterator();
        while (tableList.hasNext()) { // figli di RESOURCE
            Element tableElement = (Element) tableList.next();
            if (tableElement.getAttributeValue("ID").equals("biblioRefCluster")) {
                List childrenTABLE = tableElement.getChildren("PARAM");
                Iterator paramList = childrenTABLE.iterator();
                while (paramList.hasNext()) {  // figli di TABLE BIBLIOREF

                    Element paramElement = (Element) paramList.next();
                    if (paramElement.getAttributeValue("name").equals("title")) {
                        bibRef.setTitle(paramElement.getAttributeValue("value"));
                        if (paramElement.getChild("DESCRIPTION").getTextTrim().length() > 0) {
                            bibRef.setDescription(paramElement.getChild("DESCRIPTION").getText());
                        }
                    }
                    if (paramElement.getAttributeValue("name").equals("year")) {
                        bibRef.setYear(paramElement.getAttributeValue("value"));
                    }
                    if (paramElement.getAttributeValue("name").equals("uri") && !paramElement.getAttributeValue("value").equalsIgnoreCase("null")) {
                        bibRef.setUri(paramElement.getAttributeValue("value"));
                    }

                }
                bibRef.setId(0);
                bibRef.setObjectId(bn.getObjectId());
                bibRef.setGcAttributeId(bn.getGcAttributeId());
                bibRef.setPlsAttributeId(bn.getPlsAttributeId());
                bibRef.setStarAttributeId(bn.getStarAttributeId());
                bibRef.setUserId(bn.getUserId());
                bibRef.setImageId(bn.getImageId());
                bibRef.setDate(bn.getDate());
            }
            if (tableElement.getAttributeValue("ID").equals("paper")) {
                List childrenTable = tableElement.getChildren("PARAM");
                Iterator paramList = childrenTable.iterator();
                while (paramList.hasNext()) {  // figli di TABLE PAPER
                    Element paramElement = (Element) paramList.next();
                    if (paramElement.getAttributeValue("name").equals("paperId")) {
                        paper.setId(Integer.parseInt(paramElement.getAttributeValue("value")));
                    }
                    if (paramElement.getAttributeValue("name").equals("paperName") && paper.getId() == 0) {
                        paper.setName(paramElement.getAttributeValue("value"));
                    }
                    if (paramElement.getAttributeValue("name").equals("paperUri") && paper.getId() == 0) {
                        paper.setUri(paramElement.getAttributeValue("value"));
                    }
                }
            }
            if (tableElement.getAttributeValue("ID").equals("authors")) {
                Element tableDataElement = tableElement.getChild("DATA").getChild("TABLEDATA");
                List childrenTable = tableDataElement.getChildren("TR");
                Iterator trList = childrenTable.iterator();
                while (trList.hasNext()) {

                    Element elementTR = (Element) trList.next();
                    List childrenTR = elementTR.getChildren("TD");
                    Iterator tdList = childrenTR.iterator();
                    author = new Author(0, null, null);
                    k = 0;

                    while (tdList.hasNext()) {
                        Element elementTD = (Element) tdList.next();
                        if (k == 0) {
                            author.setId(Integer.parseInt(elementTD.getTextTrim()));
                        } else if (k == 1 && author.getId() == 0) {
                            author.setName(elementTD.getTextTrim());
                        } else if (k == 2 && elementTD.getTextTrim().length() > 0) {
                            author.setUri(elementTD.getValue());
                        }
                        k++;
                    }
                    authorList.add(author);
                }
            }
        }

        if (paper.getId()
                == 0) {
            newPaper = vogc.insertPaper(paper);
            paper.setId(newPaper);
        }

        bibRef.setPaperId(paper.getId());
        newId = vogc.insertBiblioNotes(bn);

        bibRef.setId(newId);

        vogc.insertBiblioRef(bibRef);
        Iterator aut = authorList.iterator();

        while (aut.hasNext()) {
            Author authorToInsert = (Author) aut.next();
            if (authorToInsert.getId() == 0) {
                newAuthor = vogc.insertAuthor(authorToInsert);
                authorToInsert.setId(newAuthor);
            }
            vogc.insertBiblioHasAuthor(authorToInsert.getId(), bibRef.getId());
        }
    }

    public void parseNoteUpdate(Document doc, PrintWriter out) throws Exception {

        xgen = new XMLGenerator(out);
        int noteId = 0;
        String userId = null;
        BiblioNotes bn;
        Element rootVOTable = doc.getRootElement();
        List childrenParams = rootVOTable.getChildren("PARAM");
        Iterator iterator = childrenParams.iterator();

        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();
            switch (element.getAttributeValue("name")) {
                case "noteId":
                    noteId = Integer.parseInt(element.getAttributeValue("value"));
                    break;
                case "userId":
                    userId = element.getAttributeValue("value");
                    break;
            }
        }
        bn = vogc.selectBiblioNotes(noteId);

        if (bn.getUserId().equalsIgnoreCase(userId)) {
            Element RESOURCE = rootVOTable.getChild("RESOURCE");
            Element tableElement = RESOURCE.getChild("TABLE");
            List childrenTABLE = tableElement.getChildren("PARAM");
            Iterator paramList = childrenTABLE.iterator();
            while (paramList.hasNext()) {
                Element paramElement = (Element) paramList.next();
                switch (paramElement.getAttributeValue("name")) {
                    case "title":
                        vogc.updateNotesTitle(noteId, paramElement.getAttributeValue("value"));
                        if (paramElement.getChild("DESCRIPTION").getTextTrim().length() > 0) {
                            vogc.updateNotesDescription(noteId, paramElement.getChild("DESCRIPTION").getText());
                        }
                        vogc.updateBiblioNotesDate(noteId);
                        break;
                    case "type":
                        vogc.updateNotesType(noteId, Integer.parseInt(paramElement.getAttributeValue("value")));
                        break;
                }
            }
            xgen.generateErrorReport(Messages.getError(Messages.NOTES_UPDATED));
        } else {
            xgen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
        }
    }

    public void parseBiblioRefUpdate(Document doc, PrintWriter out) throws Exception {
        xgen = new XMLGenerator(out);
        String userId = null;
        int newPaper, k, newAuthor, biblioRefId = 0;
        BiblioNotes bn;
        Paper paper = new Paper(0, null, null);
        Author author;
        List<Author> authorList = new ArrayList<>(5);

        Element rootVOTable = doc.getRootElement();
        List childrenParams = rootVOTable.getChildren("PARAM");
        Iterator iterator = childrenParams.iterator();

        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();
            switch (element.getAttributeValue("name")) {
                case "bibRefId":
                    biblioRefId = Integer.parseInt(element.getAttributeValue("value"));
                    break;
                case "userId":
                    userId = element.getAttributeValue("value");
                    break;
            }
        }
        bn = vogc.selectBiblioNotes(biblioRefId);
        if (bn.getUserId().compareTo(userId) == 0) {
            Element RESOURCE = rootVOTable.getChild("RESOURCE");
            List childrenRESOURCE = RESOURCE.getChildren("TABLE");
            Iterator tableList = childrenRESOURCE.iterator();
            while (tableList.hasNext()) { // figli di RESOURCE
                Element tableElement = (Element) tableList.next();
                if (tableElement.getAttributeValue("ID").equals("biblioRefCluster")) {
                    List childrenTABLE = tableElement.getChildren("PARAM");
                    Iterator paramList = childrenTABLE.iterator();
                    while (paramList.hasNext()) {  // figli di TABLE BIBLIOREF
                        Element paramElement = (Element) paramList.next();
                        if (paramElement.getAttributeValue("name").equals("title")) {
                            vogc.updateBiblioRefTitle(biblioRefId, paramElement.getAttributeValue("value"));
                            if (paramElement.getChild("DESCRIPTION").getTextTrim().length() > 0) {
                                vogc.updateBiblioRefDescription(biblioRefId, paramElement.getChild("DESCRIPTION").getText());
                            }
                        } else if (paramElement.getAttributeValue("name").equals("year")) {
                            vogc.updateBiblioRefYear(biblioRefId, paramElement.getAttributeValue("value"));
                        } else if (paramElement.getAttributeValue("name").equals("uri") && paramElement.getAttributeValue("value").equals("null")) {
                            vogc.updateBiblioRefUri(biblioRefId, paramElement.getAttributeValue("value"));
                        }
                    }
                    vogc.updateBiblioNotesDate(biblioRefId);
                }
                if (tableElement.getAttributeValue("ID").equals("paper")) {
                    List childrenTABLE = tableElement.getChildren("PARAM");
                    Iterator paramList = childrenTABLE.iterator();
                    while (paramList.hasNext()) {  // figli di TABLE PAPER
                        Element paramElement = (Element) paramList.next();
                        if (paramElement.getAttributeValue("name").equals("paperId")) {
                            paper.setId(Integer.parseInt(paramElement.getAttributeValue("value")));
                        }
                        if (paramElement.getAttributeValue("name").equals("paperName") && paper.getId() == 0) {
                            paper.setName(paramElement.getAttributeValue("value"));
                        }
                        if (paramElement.getAttributeValue("name").equals("paperUri") && paper.getId() == 0) {
                            paper.setUri(paramElement.getAttributeValue("value"));
                        }
                    }
                }
                if (tableElement.getAttributeValue("ID").equals("authors")) {
                    Element tableDataElement = tableElement.getChild("DATA").getChild("TABLEDATA");
                    List childrenTable = tableDataElement.getChildren("TR");
                    Iterator trList = childrenTable.iterator();
                    while (trList.hasNext()) {

                        Element elementTR = (Element) trList.next();
                        List childrenTR = elementTR.getChildren("TD");
                        Iterator tdList = childrenTR.iterator();
                        author = new Author(0, null, null);
                        k = 0;

                        while (tdList.hasNext()) {
                            Element elementTD = (Element) tdList.next();
                            if (k == 0) {
                                author.setId(Integer.parseInt(elementTD.getTextTrim()));
                            }
                            if (k == 1) {
                                author.setName(elementTD.getTextTrim());
                            }
                            if (k == 2 && elementTD.getTextTrim().length() > 0) {
                                author.setUri(elementTD.getValue());

                            }
                            k++;
                        }
                        authorList.add(author);
                    }
                }
            }

            if (paper.getId() == 0) {
                newPaper = vogc.insertPaper(paper);
                paper.setId(newPaper);
            }
            vogc.updateBiblioRefPaperId(biblioRefId, paper.getId());
            vogc.deleteBiblioHasAuthorByBiblioId(biblioRefId);

            Iterator aut = authorList.iterator();
            while (aut.hasNext()) {

                Author authorToInsert = (Author) aut.next();
                if (authorToInsert.getId() == 0) {
                    newAuthor = vogc.insertAuthor(authorToInsert);
                    authorToInsert.setId(newAuthor);
                } else if (authorToInsert.getName() != null) {

                    vogc.updateAuthorName(authorToInsert.getId(), authorToInsert.getName());
                    vogc.updateAuthorUri(authorToInsert.getId(), authorToInsert.getUri());
                }
                vogc.insertBiblioHasAuthor(authorToInsert.getId(), biblioRefId);
            }
            xgen.generateErrorReport(Messages.getError(Messages.NOTES_UPDATED));
        } else {
            xgen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
        }

    }

    public Image parseImage(Document doc, PrintWriter out) throws Exception {
        Image img = new Image(0, null, null, null, null, null, null, null, null, null, 0);

        Element rootVOTable = doc.getRootElement();
        List childrenParams = rootVOTable.getChildren("PARAM");
        Iterator iterator = childrenParams.iterator();

        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();
            switch (element.getAttributeValue("name")) {
                case "objectId":
                    img.setObjectId(element.getAttributeValue("value"));
                    break;
                case "userId":
                    img.setUserId(element.getAttributeValue("value"));
                    break;

                case "name":
                    img.setName(element.getAttributeValue("value"));
                    if (element.getChild("DESCRIPTION").getTextTrim().length() > 0) {
                        img.setDescription(element.getChild("DESCRIPTION").getText());
                    }
                    break;
                case "scale":
                    img.setScale(element.getAttributeValue("value"));
                    break;
                case "format":
                    img.setFormat(element.getAttributeValue("value"));
                    break;
                case "dimension":
                    img.setDimension(element.getAttributeValue("value"));
                    break;
                case "uri":
                    img.setUri(element.getAttributeValue("value"));
                    break;
                case "type":
                    img.setType(Integer.parseInt(element.getAttributeValue("value")));
                    break;
            }
        }
        return img;
    }

    public void parseAttributeUpdate(Document doc, PrintWriter out) throws Exception {

        xgen = new XMLGenerator(out);
        String userId = null;
        int objectType = 0;
        String attName = null, attDesc = null, attUcd = null, attDatatype = null;
        int attId, attType = 0;
        boolean userInsertAttribute = false;
        Element rootVOTABLE = doc.getRootElement();
        List childrenPARAM = rootVOTABLE.getChildren("PARAM");
        Iterator iterator = childrenPARAM.iterator();

        while (iterator.hasNext()) {
            Element element = (Element) iterator.next();
            switch (element.getAttributeValue("name")) {
                case "userId":
                    userId = element.getAttributeValue("value");
                    break;
                case "objectType":
                    objectType = Integer.parseInt(element.getAttributeValue("value"));
                    break;
                case "attName":
                    attName = element.getAttributeValue("value");
                    if (element.getChild("DESCRIPTION").getTextTrim().length() > 0) {
                        attDesc = element.getChild("DESCRIPTION").getText();
                    }
                    break;
                case "attUcd":
                    attUcd = element.getAttributeValue("value");
                    break;
                case "attDatatype":
                    attDatatype = element.getAttributeValue("value");
                    break;
                case "attType":
                    if (attType == 1) {
                        attType = Integer.parseInt(element.getAttributeValue("value"));
                    }
                    break;
            }
        }
        switch (objectType) {

            case 1:
                attId = vogc.selectGcAttributeIdByName(attName);
                GclusterHasAttribute[] gcValues = vogc.selectGcHasAttByAttributeId(attId);
                for (GclusterHasAttribute val : gcValues) {
                    if (val.getSourceId().compareTo(userId) == 0 && val.isFirst() == true) {
                        userInsertAttribute = true;
                    }
                }
                if (userInsertAttribute == true) {
                    vogc.updateGcAttributeName(attId, attName);
                    vogc.updateGcAttributeDescription(attId, attDesc);
                    vogc.updateGcAttributeDatatype(attId, attDatatype);
                    vogc.updateGcAttributeUcd(attId, attUcd);
                    vogc.updateGcAttributeType(attId, attType);
                    xgen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_UPDATED));
                } else {
                    xgen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                }

                break;
            case 2:
                attId = vogc.selectPlsAttributeIdByName(attName);
                PulsarHasAttribute[] plsValues = vogc.selectPlsHasAttByAttributeId(attId);
                for (PulsarHasAttribute val : plsValues) {
                    if (val.getSourceId().compareTo(userId) == 0 && val.isFirst() == true) {
                        userInsertAttribute = true;
                    }
                }
                if (userInsertAttribute == true) {
                    vogc.updatePlsAttributeName(attId, attName);
                    vogc.updatePlsAttributeDescription(attId, attDesc);
                    vogc.updatePlsAttributeDatatype(attId, attDatatype);
                    vogc.updatePlsAttributeUcd(attId, attUcd);
                    xgen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_UPDATED));
                } else {
                    xgen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                }

                break;
            case 3:
                attId = vogc.selectGcAttributeIdByName(attName);
                StarHasAttribute[] starValues = vogc.selectStarHasAttByAttributeId(attId);
                for (StarHasAttribute val : starValues) {
                    if (val.getSourceId().compareTo(userId) == 0 && val.isFirst() == true) {
                        userInsertAttribute = true;
                    }
                }
                if (userInsertAttribute == true) {
                    vogc.updateStarAttributeName(attId, attName);
                    vogc.updateStarAttributeDescription(attId, attDesc);
                    vogc.updateStarAttributeDatatype(attId, attDatatype);
                    vogc.updateStarAttributeUcd(attId, attUcd);
                    xgen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_UPDATED));
                } else {
                    xgen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                }

                break;
        }

    }
}
