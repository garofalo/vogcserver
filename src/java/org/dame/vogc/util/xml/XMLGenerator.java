package org.dame.vogc.util.xml;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dame.vogc.datatypes.Author;
import org.dame.vogc.datatypes.BiblioNotes;
import org.dame.vogc.datatypes.BiblioRef;
import org.dame.vogc.datatypes.Catalogue;
import org.dame.vogc.datatypes.GcAttribute;
import org.dame.vogc.datatypes.Gcluster;
import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.datatypes.Image;
import org.dame.vogc.datatypes.Notes;
import org.dame.vogc.datatypes.Paper;
import org.dame.vogc.datatypes.PlsAttribute;
import org.dame.vogc.datatypes.Pulsar;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.datatypes.Source;
import org.dame.vogc.datatypes.Star;
import org.dame.vogc.datatypes.StarAttribute;
import org.dame.vogc.datatypes.StarHasAttribute;
import org.dame.vogc.datatypes.Tag;
import org.dame.vogc.datatypes.User;
import org.dame.vogc.datatypes.VObject;
import org.dame.vogc.error.Error;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author Luca
 */
public class XMLGenerator {
//    private IVOGCAccess vogc_= new VogcAccess(WebConfig.getVogcUser(),WebConfig.getVogcPass());

    String Dir = ServerConstants.XML_TEMPLATE;
    private IVOGCAccess vogc_ = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
    private final String DIRECTORY = Dir;
    private PrintWriter out_;
    int i = 0;
    Catalogue temp;
    private Configuration cfg_;

    public XMLGenerator(PrintWriter out) throws IOException {
        out_ = out;
        cfg_ = new Configuration();
        cfg_.setDirectoryForTemplateLoading(new File(DIRECTORY));
        cfg_.setObjectWrapper(new DefaultObjectWrapper());
    }

    public void createAuthorList() throws Exception {
        Map<String, Object> allUsers = new HashMap<>(1);

        Author[] users = vogc_.selectAllAuthors();
        List<Map> authorList = new ArrayList<>(users.length);
        Map<String, Object> userInfo = new HashMap<>(users.length * 2);
//        String validatedString;

        for (Author aAuth : users) {

            userInfo.put("userId", aAuth.getId());
            userInfo.put("name", aAuth.getName());
            authorList.add(userInfo);

        }

        allUsers.put("authorInfo", authorList);
        freemarkerDo(allUsers, "authorList.ftl");
    }

    public void createBibliobyObjectInfoList(String objectId) throws Exception {
        Map<String, Object> gcData = new HashMap<>(2);
//        Map AllInfo = new HashMap();

        int papId;
        BiblioNotes b[] = vogc_.selectBiblioNotesByObjectId(objectId);

        List<Map> infoList = new ArrayList<>(b.length);
        List<Map> bibRefList = new ArrayList<>(b.length);

        for (BiblioNotes bnote : b) {
            if (bnote.getType() == 1) {

                Notes note = vogc_.selectNotes(bnote.getId());

                Map<String, Object> info = new HashMap<>(8);
                //   User usr = vogc_.selectUser(note.getUserId());
                // String nameSurname = usr.getName() + " " + usr.getSurname();

                info.put("noteId", note.getId());
                // notesInfo.put("gcHasAttId", note.getGcAttributeId());
                info.put("title", validateString(note.getTitle()));
                info.put("desc", validateString(note.getDescription()));
                info.put("type", note.getNoteType());
                info.put("userId", note.getUserId());
                //   notesInfo.put("userName", validateString(nameSurname));
                info.put("date", note.getDate());

                infoList.add(info);
                // AllInfo.put("attributes", InfoList);

            } else if (bnote.getType() == 2) {
                BiblioRef bib = vogc_.selectBiblioRef(bnote.getId());
                //   User usr = vogc_.selectUser(bib.getUserId());
                // String nameSurname = usr.getName() + " " + usr.getSurname();
                papId = vogc_.selectBiblioRefPaperId(bnote.getId());
                Map<String, Object> bibRefInfo = new HashMap<>(10);
                bibRefInfo.put("bibRefId", bib.getId());
                bibRefInfo.put("gcHasAttId", bib.getGcAttributeId());
                bibRefInfo.put("paperId", papId);
                bibRefInfo.put("paperName", validateString(vogc_.selectPaper(papId).getName()));
                bibRefInfo.put("paperUri", validateString(vogc_.selectPaper(papId).getUri()));
                bibRefInfo.put("userId", bib.getUserId());
                //bibRefInfo.put("userName", validateString(nameSurname));
                bibRefInfo.put("title", validateString(bib.getTitle()));
                if (bib.getDescription() == null) {
                    bibRefInfo.put("desc", "");
                } else {
                    bibRefInfo.put("desc", validateString(bib.getDescription()));
                }
                if (bib.getUri() == null) {
                    bibRefInfo.put("uri", "");
                } else {
                    bibRefInfo.put("uri", validateString(bib.getUri()));
                }
                bibRefInfo.put("year", bib.getYear());
                bibRefInfo.put("date", bib.getDate());
                bibRefList.add(bibRefInfo);
            }
        }
        gcData.put("info", infoList);
        gcData.put("biblioRef", bibRefList);  // riferimenti relativi agli attributi

        freemarkerDo(gcData, "biblionotes.ftl");
    }

    public void createBibliobyUserInfoList(String objectId) throws Exception {
        Map<String, Object> gcData = new HashMap<>(2);
//        Map AllInfo = new HashMap();
        Map<String, Object> info;

        int papId;
        BiblioNotes b[] = vogc_.selectBiblioNotesByUserId(objectId);
        List<Map> infoList = new ArrayList<>(b.length);
        List<Map> bibRefList = new ArrayList<>(b.length);
        Map<String, Object> bibRefInfo;

        for (BiblioNotes bnote : b) {
            if (bnote.getType() == 1) {

                Notes note = vogc_.selectNotes(bnote.getId());

                info = new HashMap<>(8);

                info.put("noteId", note.getId());

                info.put("title", validateString(note.getTitle()));
                info.put("desc", validateString(note.getDescription()));
                info.put("type", note.getNoteType());
                info.put("userId", note.getUserId());

                info.put("date", note.getDate());

                infoList.add(info);

            } else if (bnote.getType() == 2) {
                BiblioRef bib = vogc_.selectBiblioRef(bnote.getId());

                papId = vogc_.selectBiblioRefPaperId(bnote.getId());

                bibRefInfo = new HashMap<>(14);
                bibRefInfo.put("bibRefId", bib.getId());
                bibRefInfo.put("gcHasAttId", bib.getGcAttributeId());
                bibRefInfo.put("paperId", papId);
                bibRefInfo.put("paperName", validateString(vogc_.selectPaper(papId).getName()));
                bibRefInfo.put("paperUri", validateString(vogc_.selectPaper(papId).getUri()));
                bibRefInfo.put("userId", bib.getUserId());

                bibRefInfo.put("title", validateString(bib.getTitle()));
                if (bib.getDescription() == null) {
                    bibRefInfo.put("desc", "");
                } else {
                    bibRefInfo.put("desc", validateString(bib.getDescription()));
                }
                if (bib.getUri() == null) {
                    bibRefInfo.put("uri", "");
                } else {
                    bibRefInfo.put("uri", validateString(bib.getUri()));
                }
                bibRefInfo.put("year", bib.getYear());
                bibRefInfo.put("date", bib.getDate());
                bibRefList.add(bibRefInfo);
            }
        }
        gcData.put("info", infoList);
        gcData.put("biblioRef", bibRefList);  // riferimenti relativi agli attributi

        freemarkerDo(gcData, "biblionotes.ftl");
    }

    public void createGclusterInfoList(String gclusterId) throws Exception {
        Gcluster gc = vogc_.selectGcluster(gclusterId);  // seleziono l'ammasso globulare a partire dall'id
        Map<String, Object> gcData = new HashMap<>(20);
        gcData.put("Id", gclusterId);
        if (gc.getName() == null) {
            gcData.put("Name", "");
        } else {
            gcData.put("Name", gc.getName());
        }
        gcData.put("type", gc.getClusterType());
        gcData.put("gclusterId", gc.getClusterId());
        int j, papId;

        GclusterHasAttribute att[] = vogc_.selectGcHasAttByGclusterId(gclusterId);

        List<Map> attList = new ArrayList<>(att.length);
        List<Map> bibRefList = null;
        List<Map> notesList = null;
        List<Map> bibRefListCluster;
        List<Map> notesListCluster;
        List<Map> imagesList;
        List<Map> notesListImage = null;
        List<Map> bibRefListImage = null;
        List<Map> tagClusterList;
        List<Map> tagImageList;
        List<Map> pulsarList;
        List<Map> starList;

        List<Map> refAuthorList;

        Map<String, Object> attInfo;
        Map<String, Object> bibRefInfo;
        Map<String, Object> notesInfo;
        Map<String, Object> bibRefInfoCluster;
        Map<String, Object> notesInfoCluster;
        Map<String, Object> imageInfo;
        Map<String, Object> notesInfoImage;
        Map<String, Object> bibRefInfoImage;
        Map<String, Object> tagCluster;
        Map<String, Object> tagImage;
        Map<String, Object> pulsarInfo;
        Map<String, Object> starInfo;

        for (GclusterHasAttribute a : att) {
            attInfo = new HashMap<>(15);
            GcAttribute attr = vogc_.selectGcAttribute(a.getGcAttId());
            attInfo.put("attributeId", attr.getId());
            attInfo.put("attributeName", attr.getName());
            if (attr.getDescription() == null) {
                attInfo.put("attributeDesc", "");
            } else {
                attInfo.put("attributeDesc", validateString(attr.getDescription()));
            }
            attInfo.put("attributeDatatype", attr.getDatatype());

            if (attr.getUcd() == null) {
                attInfo.put("attributeUcd", "");
            } else {
                attInfo.put("attributeUcd", attr.getUcd());
            }

            attInfo.put("attributeType", attr.getType());

            Source s = vogc_.selectSource(a.getSourceId());
            attInfo.put("sourceId", validateString(s.getId()));
            attInfo.put("sourceType", s.getType());
            if (s.getType() == 2) {  // se src è un utente recupero nome e cognome
                User usr = vogc_.selectUser(s.getId());
                String nameSurname = usr.getName() + " " + usr.getSurname();
                attInfo.put("userName", validateString(nameSurname));
            } else {
                attInfo.put("userName", "");
            }

            attInfo.put("value", validateString(a.getValue()));
            attInfo.put("date", a.getDate());
            if (a.isFirst() == true) {
                attInfo.put("isFirst", "1");
            } else {
                attInfo.put("isFirst", "0");
            }
            if (attr.isPrimaryAtt() == true) {
                attInfo.put("isPrimary", "1");
            } else {
                attInfo.put("isPrimary", "0");
            }
            attInfo.put("gcHasAttId", a.getId());
            attList.add(attInfo);

            BiblioNotes b[] = vogc_.selectBiblioNotesByGcAttributeId(a.getId());
            bibRefList = new ArrayList<>(b.length);
            notesList = new ArrayList<>(b.length);
            for (BiblioNotes bnote : b) {
                if (bnote.getType() == 1) {

                    Notes note = vogc_.selectNotes(bnote.getId());
                    User usr = vogc_.selectUser(note.getUserId());
                    String nameSurname = usr.getName() + " " + usr.getSurname();
                    notesInfo = new HashMap<>(10);
                    notesInfo.put("noteId", note.getId());
                    notesInfo.put("gcHasAttId", note.getGcAttributeId());
                    notesInfo.put("title", validateString(note.getTitle()));
                    notesInfo.put("desc", validateString(note.getDescription()));
                    notesInfo.put("type", note.getNoteType());
                    notesInfo.put("userId", note.getUserId());
                    notesInfo.put("userName", validateString(nameSurname));
                    notesInfo.put("date", note.getDate());
                    notesList.add(notesInfo);

                } else if (bnote.getType() == 2) {
                    BiblioRef bib = vogc_.selectBiblioRef(bnote.getId());
                    User usr = vogc_.selectUser(bib.getUserId());
                    String nameSurname = usr.getName() + " " + usr.getSurname();
                    papId = vogc_.selectBiblioRefPaperId(bnote.getId());

                    bibRefInfo = new HashMap<>(15);
                    bibRefInfo.put("bibRefId", bib.getId());
                    bibRefInfo.put("gcHasAttId", bib.getGcAttributeId());
                    bibRefInfo.put("paperId", papId);
                    bibRefInfo.put("paperName", validateString(vogc_.selectPaper(papId).getName()));
                    bibRefInfo.put("paperUri", validateString(vogc_.selectPaper(papId).getUri()));
                    bibRefInfo.put("userId", bib.getUserId());
                    bibRefInfo.put("userName", validateString(nameSurname));
                    bibRefInfo.put("title", validateString(bib.getTitle()));
                    if (bib.getDescription() == null) {
                        bibRefInfo.put("desc", "");
                    } else {
                        bibRefInfo.put("desc", validateString(bib.getDescription()));
                    }
                    if (bib.getUri() == null) {
                        bibRefInfo.put("uri", "");
                    } else {
                        bibRefInfo.put("uri", validateString(bib.getUri()));
                    }
                    bibRefInfo.put("year", bib.getYear());
                    bibRefInfo.put("date", bib.getDate());
                    bibRefList.add(bibRefInfo);
                }
            }
        }
        BiblioNotes bn[] = vogc_.selectBiblioNotesByObjectId(gclusterId);
        bibRefListCluster = new ArrayList<>(bn.length);
        notesListCluster = new ArrayList<>(bn.length);
        for (BiblioNotes b : bn) {
            User usr = vogc_.selectUser(b.getUserId());
            String nameSurname = usr.getName() + " " + usr.getSurname();
            if (b.getType() == 1) {
                Notes note = vogc_.selectNotes(b.getId());
                notesInfoCluster = new HashMap<>(10);
                notesInfoCluster.put("noteId", note.getId());
                notesInfoCluster.put("title", note.getTitle());
                notesInfoCluster.put("desc", note.getDescription());
                notesInfoCluster.put("type", note.getNoteType());
                notesInfoCluster.put("userId", note.getUserId());
                notesInfoCluster.put("userName", validateString(nameSurname));
                notesInfoCluster.put("date", note.getDate());
                notesListCluster.add(notesInfoCluster);
            } else if (b.getType() == 2) {
                BiblioRef bib = vogc_.selectBiblioRef(b.getId());
                papId = vogc_.selectBiblioRefPaperId(b.getId());

                bibRefInfoCluster = new HashMap<>(15);
                bibRefInfoCluster.put("bibRefId", bib.getId());
                bibRefInfoCluster.put("paperId", papId);
                bibRefInfoCluster.put("paperName", vogc_.selectPaper(papId).getName());
                bibRefInfoCluster.put("paperUri", vogc_.selectPaper(papId).getUri());
                bibRefInfoCluster.put("userId", bib.getUserId());
                bibRefInfoCluster.put("userName", validateString(nameSurname));

                bibRefInfoCluster.put("title", validateString(bib.getTitle()));
                if (bib.getDescription() == null) {
                    bibRefInfoCluster.put("desc", "");
                } else {
                    bibRefInfoCluster.put("desc", validateString(bib.getDescription()));
                }
                if (bib.getUri() == null) {
                    bibRefInfoCluster.put("uri", "");
                } else {
                    bibRefInfoCluster.put("uri", validateString(bib.getUri()));
                }
                bibRefInfoCluster.put("year", bib.getYear());
                bibRefInfoCluster.put("date", bib.getDate());
                bibRefListCluster.add(bibRefInfoCluster);
            }

        }
        Image images[] = vogc_.selectImageByObjectId(gclusterId);
        imagesList = new ArrayList<>(images.length);
        tagImageList = new ArrayList<>(images.length);
        for (Image img : images) {
            User usr = vogc_.selectUser(img.getUserId());
            String nameSurname = usr.getName() + " " + usr.getSurname();
            imageInfo = new HashMap<>(15);
            imageInfo.put("imageId", img.getId());
            imageInfo.put("userId", img.getUserId());
            imageInfo.put("userName", validateString(nameSurname));
            if (img.getName() == null) {
                imageInfo.put("name", "");
            } else {
                imageInfo.put("name", img.getName());
            }
            if (img.getDescription() == null) {
                imageInfo.put("desc", "");
            } else {
                imageInfo.put("desc", validateString(img.getDescription()));
            }
            if (img.getScale() == null) {
                imageInfo.put("scale", "");
            } else {
                imageInfo.put("scale", validateString(img.getScale()));
            }
            if (img.getFormat() == null) {
                imageInfo.put("format", "");
            } else {
                imageInfo.put("format", validateString(img.getFormat()));
            }
            imageInfo.put("dim", img.getDimension());
            imageInfo.put("uri", img.getUri());
            imageInfo.put("date", img.getDate());
            imagesList.add(imageInfo);

            int tagsInImage[] = vogc_.selectTagsInImage(img.getId());
            for (int k = 0; k < tagsInImage.length; k++) {
                Tag tag = vogc_.selectTag(tagsInImage[k]);
                usr = vogc_.selectUser(vogc_.selectUserByTag(tag.getId()));
                nameSurname = usr.getName() + " " + usr.getSurname();
                tagImage = new HashMap<>(8);
                tagImage.put("imageId", img.getId());
                tagImage.put("tagId", tag.getId());
                tagImage.put("userId", usr.getId());
                tagImage.put("userName", validateString(nameSurname));
                tagImage.put("tagName", validateString(tag.getName()));

                tagImageList.add(tagImage);
            }

            BiblioNotes bnImages[] = vogc_.selectBiblioNotesByImageId(img.getId());
            bibRefListImage = new ArrayList<>(bnImages.length);
            notesListImage = new ArrayList<>(bnImages.length);
            for (BiblioNotes b : bnImages) {
                usr = vogc_.selectUser(b.getUserId());
                nameSurname = usr.getName() + " " + usr.getSurname();
                if (b.getType() == 1) {
                    Notes note = vogc_.selectNotes(b.getId());
                    notesInfoImage = new HashMap<>(10);
                    notesInfoImage.put("noteId", note.getId());
                    notesInfoImage.put("imageId", note.getImageId());
                    notesInfoImage.put("title", validateString(note.getTitle()));
                    notesInfoImage.put("desc", validateString(note.getDescription()));
                    notesInfoImage.put("type", note.getNoteType());
                    notesInfoImage.put("userId", note.getUserId());
                    notesInfoImage.put("userName", validateString(nameSurname));
                    notesInfoImage.put("date", note.getDate());
                    notesListImage.add(notesInfoImage);

                } else if (b.getType() == 2) {
                    BiblioRef bib = vogc_.selectBiblioRef(b.getId());
                    papId = vogc_.selectBiblioRefPaperId(b.getId());

                    bibRefInfoImage = new HashMap<>(15);
                    bibRefInfoImage.put("bibRefId", bib.getId());
                    bibRefInfoImage.put("imageId", bib.getImageId());
                    bibRefInfoImage.put("paperId", papId);
                    bibRefInfoImage.put("paperName", validateString(vogc_.selectPaper(papId).getName()));
                    bibRefInfoImage.put("paperUri", validateString(vogc_.selectPaper(papId).getUri()));
                    bibRefInfoImage.put("userId", bib.getUserId());
                    bibRefInfoImage.put("userName", validateString(nameSurname));
                    bibRefInfoImage.put("title", bib.getTitle());
                    if (bib.getDescription() == null) {
                        bibRefInfoImage.put("desc", "");
                    } else {
                        bibRefInfoImage.put("desc", validateString(bib.getDescription()));
                    }
                    if (bib.getUri() == null) {
                        bibRefInfoImage.put("uri", "");
                    } else {
                        bibRefInfoImage.put("uri", validateString(bib.getUri()));
                    }
                    bibRefInfoImage.put("year", bib.getYear());
                    bibRefInfoImage.put("date", bib.getDate());
                    bibRefListImage.add(bibRefInfoImage);
                }
            }
        }

        j = 0;
        int idSize = bibRefList.size() + bibRefListCluster.size() + bibRefListImage.size();
        List<Object> idRef = new ArrayList<>(idSize);
        while (j < bibRefList.size()) {  // recupero gli id dei riferimenti relativi agli attributi
            idRef.add(bibRefList.get(j).get("bibRefId"));
            j++;
        }
        j = 0;
        while (j < bibRefListCluster.size()) { // recupero gli id dei riferimenti relativi al singolo ammasso
            idRef.add(bibRefListCluster.get(j).get("bibRefId"));
            j++;
        }
        j = 0;

        while (j < bibRefListImage.size()) { // recupero gli id dei riferimenti relativi ad un'immagine
            idRef.add(bibRefListImage.get(j).get("bibRefId"));
            j++;
        }
        Map<String, Object> refAuthor;
        refAuthorList = new ArrayList<>(idRef.size() * 5);
        int authorsId[];
        for (j = 0; j < idRef.size(); j++) {
            authorsId = vogc_.selectAuthorsInBiblioRef((Integer) idRef.get(j));  // recupero gli id degli autori
            for (int k = 0; k < authorsId.length; k++) {
                refAuthor = new HashMap<>(5);
                Author a = vogc_.selectAuthor(authorsId[k]);
                refAuthor.put("biblioRefId", idRef.get(j));
                refAuthor.put("authorId", a.getId());
                refAuthor.put("authorName", validateString(a.getName()));
                if (a.getUri() == null) {
                    refAuthor.put("authorUri", "");
                } else {
                    refAuthor.put("authorUri", validateString(a.getUri()));
                }
                refAuthorList.add(refAuthor);
            }
        }
        int tagsId[] = vogc_.selectTagsInVObject(gclusterId);
        tagClusterList = new ArrayList<>(tagsId.length);
        for (int k = 0; k < tagsId.length; k++) {
            Tag tag = vogc_.selectTag(tagsId[k]);
            User usr = vogc_.selectUser(vogc_.selectUserByTag(tag.getId()));
            String nameSurname = usr.getName() + " " + usr.getSurname();
            tagCluster = new HashMap<>(5);
            tagCluster.put("tagId", tag.getId());
            tagCluster.put("userId", vogc_.selectUserByTag(tag.getId()));
            tagCluster.put("userName", validateString(nameSurname));
            tagCluster.put("tagName", validateString(tag.getName()));
            tagClusterList.add(tagCluster);
        }
        Pulsar pls[] = vogc_.selectPulsarsByClusterId(gclusterId);
        pulsarList = new ArrayList<>(pls.length);
        for (Pulsar p : pls) {
            pulsarInfo = new HashMap<>(5);
            pulsarInfo.put("pulsarId", p.getId());
            pulsarList.add(pulsarInfo);
        }
        Star star[] = vogc_.selectStarsByClusterId(gclusterId);
        starList = new ArrayList<>(star.length);
        for (Star s : star) {
            starInfo = new HashMap<>(5);
            starInfo.put("starId", s.getId());
            starList.add(starInfo);
        }

        gcData.put("AttributeInfo", attList);  // descrizione degli attibuti
        gcData.put("notes", notesList);  // note relative agli attributi
        gcData.put("biblioRef", bibRefList);  // riferimenti relativi agli attributi
        gcData.put("notesCluster", notesListCluster); // note relative al singolo ammasso
        gcData.put("biblioRefCluster", bibRefListCluster); // riferimenti relativi al singolo ammasso
        gcData.put("imageInfo", imagesList); // info relative alle immagini
        gcData.put("notesImage", notesListImage); // note relative alle immagini
        gcData.put("biblioRefImage", bibRefListImage);  // riferimenti bibliografici relativi alle immagini
        gcData.put("refAuthor", refAuthorList); // corrispondenze fra riferimenti e autori
        gcData.put("tagCluster", tagClusterList);  // elenco tag associati all'ammasso
        gcData.put("tagImage", tagImageList); // elenco tag associati alle immagini
        gcData.put("pulsarList", pulsarList); // elenco pulsar appartenenti all'ammasso
        gcData.put("starList", starList);   // elenco stelle appartenenti all'ammasso

        freemarkerDo(gcData, "gcluster.ftl");

    }

    public void createImageListByObjectId(String objectId) throws Exception {
        Image image[] = vogc_.selectImageByObjectId(objectId);

        Map<String, Object> gcData;
        List<Map> galList = new ArrayList<>(image.length);

        Map<String, Object> info = new HashMap<>(8);
        info.put("stringSearched", objectId);
        info.put("Id", objectId);
        info.put("Name", "");
        info.put("type", "");
        for (Image img : image) {
            gcData = new HashMap<>(10);
            gcData.put("imageId", img.getId());
            gcData.put("userId", img.getUserId());

            if (img.getName() == null) {
                gcData.put("name", "");
            } else {
                gcData.put("name", img.getName());
            }
            if (img.getDescription() == null) {
                gcData.put("desc", "");
            } else {
                gcData.put("desc", validateString(img.getDescription()));
            }
            if (img.getScale() == null) {
                gcData.put("scale", "");
            } else {
                gcData.put("scale", validateString(img.getScale()));
            }
            if (img.getFormat() == null) {
                gcData.put("format", "");
            } else {
                gcData.put("format", validateString(img.getFormat()));
            }
            gcData.put("dim", img.getDimension());
            gcData.put("uri", img.getUri());
            gcData.put("date", img.getDate());
            galList.add(gcData);

        }

        info.put("imgList", galList);
        freemarkerDo(info, "SearchImageResult.ftl");

    }

    public void createImageListByUserId(String objectId) throws Exception {
        Image image[] = vogc_.selectImageByUserId(objectId);

        Map<String, Object> gcData;
        List<Map> galList = new ArrayList<>(image.length);
//        List tagList = new ArrayList<>();
        Map<String, Object> info = new HashMap<>(8);
        info.put("stringSearched", objectId);
        info.put("Id", objectId);
        info.put("Name", "");
        info.put("type", "");
        for (Image img : image) {
            gcData = new HashMap<>(15);
            gcData.put("imageId", img.getId());
            gcData.put("userId", img.getUserId());

            if (img.getName() == null) {
                gcData.put("name", "");
            } else {
                gcData.put("name", img.getName());
            }
            if (img.getDescription() == null) {
                gcData.put("desc", "");
            } else {
                gcData.put("desc", validateString(img.getDescription()));
            }
            if (img.getScale() == null) {
                gcData.put("scale", "");
            } else {
                gcData.put("scale", validateString(img.getScale()));
            }
            if (img.getFormat() == null) {
                gcData.put("format", "");
            } else {
                gcData.put("format", validateString(img.getFormat()));
            }
            gcData.put("dim", img.getDimension());
            gcData.put("uri", img.getUri());
            gcData.put("date", img.getDate());
            galList.add(gcData);

        }

        info.put("imgList", galList);
        freemarkerDo(info, "SearchImageResult.ftl");

    }

    public void createUsersList() throws Exception {

        User[] users = vogc_.selectAllUsers();
        Map<String, Object> allUsers = new HashMap<>(1);
        Map<String, String> userInfo;
        List<Map> userList = new ArrayList<>(users.length);
        String validatedString;

        for (User aUser : users) {
            userInfo = new HashMap<>(8);
            if (aUser.getId().compareTo("0000000000") != 0) {

                userInfo.put("userId", aUser.getId());
                userInfo.put("name", aUser.getName());
                userInfo.put("surname", aUser.getSurname());
                if (aUser.getAffiliation() == null) {
                    userInfo.put("motivation", "");
                } else {
                    validatedString = validateString(aUser.getAffiliation());
                    userInfo.put("motivation", validatedString);
                }
                if (aUser.isActive() == true) {
                    userInfo.put("active", "1");
                } else {
                    userInfo.put("active", "0");
                }
                userList.add(userInfo);
            }
        }

        allUsers.put("userInfo", userList);
        freemarkerDo(allUsers, "usersList.ftl");
    }

    public void createPulsarInfoList(String pulsarId) throws Exception {
        Map<String, Object> plsData = new HashMap<>(15);
        Map<String, Object> attInfo;
        Map<String, Object> notesInfo;
        Map<String, Object> bibRefInfo;
        Map<String, Object> notesInfoPulsar;
        Map<String, Object> bibRefInfoPulsar;
        Map<String, Object> imageInfo;
        Map<String, Object> tagImage;
        Map<String, Object> notesInfoImage;
        Map<String, Object> bibRefInfoImage;
        Map<String, Object> tagPulsar;

        List<Map> attList;
        List<Map> notesList = new ArrayList<>(0);
        List<Map> bibRefList = new ArrayList<>(0);
        List<Map> notesListPulsar = new ArrayList<>(0);
        List<Map> bibRefListPulsar = new ArrayList<>(0);
        List<Map> imagesList = new ArrayList<>(0);
        List<Map> tagImageList = new ArrayList<>(0);
        List<Map> notesListImage = new ArrayList<>(0);
        List<Map> bibRefListImage = new ArrayList<>(0);
        List<Map> tagPulsarList = new ArrayList<>(0);
        int papId, j;

        Pulsar pulsar = vogc_.selectPulsar(pulsarId);
        plsData.put("Id", pulsarId);
        plsData.put("gclusterId", pulsar.getGclusterId());

        PulsarHasAttribute attributes[] = vogc_.selectPlsHasAttByPulsarId(pulsarId);
        attList = new ArrayList<>(attributes.length);
        for (PulsarHasAttribute att : attributes) {
            PlsAttribute plsAtt = vogc_.selectPlsAttribute(att.getPlsAttId());

            attInfo = new HashMap<>(20);
            attInfo.put("attributeId", plsAtt.getId());
            attInfo.put("attributeName", plsAtt.getName());
            if (plsAtt.getDescription() == null) {
                attInfo.put("attributeDesc", "");
            } else {
                attInfo.put("attributeDesc", validateString(plsAtt.getDescription()));
            }
            attInfo.put("attributeDatatype", plsAtt.getDatatype());
            if (plsAtt.getUcd() == null) {
                attInfo.put("attributeUcd", "");
            } else {
                attInfo.put("attributeUcd", plsAtt.getUcd());
            }

            Source s = vogc_.selectSource(att.getSourceId());
            attInfo.put("sourceId", validateString(s.getId()));
            attInfo.put("sourceType", s.getType());
            if (s.getType() == 2) {  // se src è un utente recupero nome e cognome
                User usr = vogc_.selectUser(s.getId());
                String nameSurname = usr.getName() + " " + usr.getSurname();
                attInfo.put("userName", validateString(nameSurname));
            } else {
                attInfo.put("userName", "");
            }

            attInfo.put("value", validateString(att.getValue()));
            attInfo.put("date", att.getDate());
            if (att.isFirst() == true) {
                attInfo.put("isFirst", "1");
            } else {
                attInfo.put("isFirst", "0");
            }
            if (plsAtt.isPrimaryAtt() == true) {
                attInfo.put("isPrimary", "1");
            } else {
                attInfo.put("isPrimary", "0");
            }
            attInfo.put("plsHasAttId", att.getId());

            attList.add(attInfo);

            BiblioNotes b[] = vogc_.selectBiblioNotesByPlsAttributeId(att.getId());
            notesList = new ArrayList<>(b.length);
            bibRefList = new ArrayList<>(b.length);

            for (BiblioNotes bnote : b) {
                User usr = vogc_.selectUser(bnote.getUserId());
                String nameSurname = usr.getName() + " " + usr.getSurname();
                if (bnote.getType() == 1) {

                    Notes note = vogc_.selectNotes(bnote.getId());
                    notesInfo = new HashMap<>(10);
                    notesInfo.put("noteId", note.getId());
                    notesInfo.put("plsHasAttId", note.getPlsAttributeId());
                    notesInfo.put("title", validateString(note.getTitle()));
                    notesInfo.put("desc", validateString(note.getDescription()));
                    notesInfo.put("type", note.getNoteType());
                    notesInfo.put("userId", note.getUserId());
                    notesInfo.put("userName", validateString(nameSurname));
                    notesInfo.put("date", note.getDate());
                    notesList.add(notesInfo);

                } else if (bnote.getType() == 2) {
                    BiblioRef bib = vogc_.selectBiblioRef(bnote.getId());
                    papId = vogc_.selectBiblioRefPaperId(bnote.getId());

                    bibRefInfo = new HashMap<>(15);
                    bibRefInfo.put("bibRefId", bib.getId());
                    bibRefInfo.put("plsHasAttId", bib.getPlsAttributeId());
                    bibRefInfo.put("paperId", papId);
                    bibRefInfo.put("paperName", validateString(vogc_.selectPaper(papId).getName()));
                    bibRefInfo.put("paperUri", validateString(vogc_.selectPaper(papId).getUri()));
                    bibRefInfo.put("userId", bib.getUserId());
                    bibRefInfo.put("userName", validateString(nameSurname));
                    bibRefInfo.put("title", validateString(bib.getTitle()));
                    if (bib.getDescription() == null) {
                        bibRefInfo.put("desc", "");
                    } else {
                        bibRefInfo.put("desc", validateString(bib.getDescription()));
                    }
                    if (bib.getUri() == null) {
                        bibRefInfo.put("uri", "");
                    } else {
                        bibRefInfo.put("uri", validateString(bib.getUri()));
                    }
                    bibRefInfo.put("year", bib.getYear());
                    bibRefInfo.put("date", bib.getDate());
                    bibRefList.add(bibRefInfo);
                }
            }

        }

        BiblioNotes bn[] = vogc_.selectBiblioNotesByObjectId(pulsarId);
        for (BiblioNotes b : bn) {
            User usr = vogc_.selectUser(b.getUserId());
            String nameSurname = usr.getName() + " " + usr.getSurname();
            if (b.getType() == 1) {
                Notes note = vogc_.selectNotes(b.getId());
                notesInfoPulsar = new HashMap<>(10);
                notesInfoPulsar.put("noteId", note.getId());
                notesInfoPulsar.put("pulsarId", note.getObjectId());
                notesInfoPulsar.put("title", validateString(note.getTitle()));
                notesInfoPulsar.put("desc", validateString(note.getDescription()));
                notesInfoPulsar.put("type", note.getNoteType());
                notesInfoPulsar.put("userId", note.getUserId());
                notesInfoPulsar.put("userName", validateString(nameSurname));
                notesListPulsar.add(notesInfoPulsar);
            } else if (b.getType() == 2) {
                BiblioRef bib = vogc_.selectBiblioRef(b.getId());
                papId = vogc_.selectBiblioRefPaperId(b.getId());

                bibRefInfoPulsar = new HashMap<>(15);
                bibRefInfoPulsar.put("bibRefId", bib.getId());
                bibRefInfoPulsar.put("paperId", papId);
                bibRefInfoPulsar.put("paperName", validateString(vogc_.selectPaper(papId).getName()));
                bibRefInfoPulsar.put("paperUri", vogc_.selectPaper(papId).getUri());
                bibRefInfoPulsar.put("userId", bib.getUserId());
                bibRefInfoPulsar.put("userName", validateString(nameSurname));
                bibRefInfoPulsar.put("title", validateString(bib.getTitle()));
                if (bib.getDescription() == null) {
                    bibRefInfoPulsar.put("desc", "");
                } else {
                    bibRefInfoPulsar.put("desc", validateString(bib.getDescription()));
                }
                if (bib.getUri() == null) {
                    bibRefInfoPulsar.put("uri", "");
                } else {
                    bibRefInfoPulsar.put("uri", validateString(bib.getUri()));
                }
                bibRefInfoPulsar.put("year", bib.getYear());
                bibRefInfoPulsar.put("date", bib.getDate());
                bibRefListPulsar.add(bibRefInfoPulsar);
            }

        }
        Image images[] = vogc_.selectImageByObjectId(pulsarId);
        for (Image img : images) {
            User usr = vogc_.selectUser(img.getUserId());
            String nameSurname = usr.getName() + " " + usr.getSurname();
            imageInfo = new HashMap<>(15);
            imageInfo.put("imageId", img.getId());
            imageInfo.put("userId", img.getUserId());
            imageInfo.put("userName", validateString(nameSurname));
            if (img.getName() == null) {
                imageInfo.put("name", "");
            } else {
                imageInfo.put("name", validateString(img.getName()));
            }
            if (img.getDescription() == null) {
                imageInfo.put("desc", "");
            } else {
                imageInfo.put("desc", validateString(img.getDescription()));
            }
            if (img.getScale() == null) {
                imageInfo.put("scale", "");
            } else {
                imageInfo.put("scale", validateString(img.getScale()));
            }
            if (img.getFormat() == null) {
                imageInfo.put("format", "");
            } else {
                imageInfo.put("format", validateString(img.getFormat()));
            }
            imageInfo.put("dim", validateString(img.getDimension()));
            imageInfo.put("uri", validateString(img.getUri()));
            imageInfo.put("date", img.getDate());
            imagesList.add(imageInfo);

            int tagsInImage[] = vogc_.selectTagsInImage(img.getId());
            for (int k = 0; k < tagsInImage.length; k++) {
                Tag tag = vogc_.selectTag(tagsInImage[k]);
                usr = vogc_.selectUser(vogc_.selectUserByTag(tag.getId()));
                nameSurname = usr.getName() + " " + usr.getSurname();
                tagImage = new HashMap<>(5);
                tagImage.put("imageId", img.getId());
                tagImage.put("tagId", tag.getId());
                tagImage.put("userId", vogc_.selectUserByTag(tag.getId()));
                tagImage.put("userName", validateString(nameSurname));
                tagImage.put("tagName", validateString(tag.getName()));

                tagImageList.add(tagImage);
            }
            BiblioNotes bnImages[] = vogc_.selectBiblioNotesByImageId(img.getId());
            for (BiblioNotes b : bnImages) {
                usr = vogc_.selectUser(b.getUserId());
                nameSurname = usr.getName() + " " + usr.getSurname();
                if (b.getType() == 1) {
                    Notes note = vogc_.selectNotes(b.getId());
                    notesInfoImage = new HashMap<>(10);
                    notesInfoImage.put("noteId", note.getId());
                    notesInfoImage.put("imageId", note.getImageId());
                    notesInfoImage.put("title", validateString(note.getTitle()));
                    notesInfoImage.put("desc", validateString(note.getDescription()));
                    notesInfoImage.put("type", note.getNoteType());
                    notesInfoImage.put("userId", note.getUserId());
                    notesInfoImage.put("userName", validateString(nameSurname));
                    notesInfoImage.put("date", note.getDate());
                    notesListImage.add(notesInfoImage);

                } else if (b.getType() == 2) {
                    BiblioRef bib = vogc_.selectBiblioRef(b.getId());
                    papId = vogc_.selectBiblioRefPaperId(b.getId());

                    bibRefInfoImage = new HashMap<>(15);
                    bibRefInfoImage.put("bibRefId", bib.getId());
                    bibRefInfoImage.put("imageId", bib.getImageId());
                    bibRefInfoImage.put("paperId", papId);
                    bibRefInfoImage.put("paperName", validateString(vogc_.selectPaper(papId).getName()));
                    bibRefInfoImage.put("paperUri", validateString(vogc_.selectPaper(papId).getUri()));
                    bibRefInfoImage.put("userId", bib.getUserId());
                    bibRefInfoImage.put("userName", validateString(nameSurname));
                    bibRefInfoImage.put("title", bib.getTitle());
                    if (bib.getDescription() == null) {
                        bibRefInfoImage.put("desc", "");
                    } else {
                        bibRefInfoImage.put("desc", validateString(bib.getDescription()));
                    }
                    if (bib.getUri() == null) {
                        bibRefInfoImage.put("uri", "");
                    } else {
                        bibRefInfoImage.put("uri", validateString(bib.getUri()));
                    }
                    bibRefInfoImage.put("year", bib.getYear());
                    bibRefInfoImage.put("date", bib.getDate());
                    bibRefListImage.add(bibRefInfoImage);
                }
            }
        }

        j = 0;
        int idSize = bibRefList.size() + bibRefListPulsar.size() + bibRefListImage.size();
        List<Object> idRef = new ArrayList<>(idSize);
        while (j < bibRefList.size()) {  // recupero gli id dei riferimenti relativi agli attributi
            idRef.add(bibRefList.get(j).get("bibRefId"));
            j++;
        }
        j = 0;
        while (j < bibRefListPulsar.size()) { // recupero gli id dei riferimenti relativi al singolo ammasso
            idRef.add(bibRefListPulsar.get(j).get("bibRefId"));
            j++;
        }
        j = 0;

        while (j < bibRefListImage.size()) { // recupero gli id dei riferimenti relativi ad un'immagine
            idRef.add(bibRefListImage.get(j).get("bibRefId"));
            j++;
        }
        Map<String, Object> refAuthor;
        List<Map> refAuthorList = new ArrayList<>(idRef.size() * 5);
        int authorsId[];
        for (j = 0; j < idRef.size(); j++) {
            authorsId = vogc_.selectAuthorsInBiblioRef((Integer) idRef.get(j));  // recupero gli id degli autori
            for (int k = 0; k < authorsId.length; k++) {
                refAuthor = new HashMap<>(5);
                Author a = vogc_.selectAuthor(authorsId[k]);
                refAuthor.put("biblioRefId", idRef.get(j));
                refAuthor.put("authorId", a.getId());
                refAuthor.put("authorName", validateString(a.getName()));
                if (a.getUri() == null) {
                    refAuthor.put("authorUri", "");
                } else {
                    refAuthor.put("authorUri", validateString(a.getUri()));
                }
                refAuthorList.add(refAuthor);
            }
        }
        int tagsId[] = vogc_.selectTagsInVObject(pulsarId);
        for (int k = 0; k < tagsId.length; k++) {
            User usr = vogc_.selectUser(vogc_.selectUserByTag(tagsId[k]));
            String nameSurname = usr.getName() + " " + usr.getSurname();
            Tag tag = vogc_.selectTag(tagsId[k]);
            tagPulsar = new HashMap<>(5);
            tagPulsar.put("tagId", tag.getId());
            tagPulsar.put("userId", vogc_.selectUserByTag(tag.getId()));
            tagPulsar.put("userName", validateString(nameSurname));
            tagPulsar.put("tagName", validateString(tag.getName()));
            tagPulsarList.add(tagPulsar);
        }
        plsData.put("attributeInfo", attList);  // descrizione degli attibuti
        plsData.put("notes", notesList);  // note relative agli attributi
        plsData.put("biblioRef", bibRefList);  // riferimenti relativi agli attributi
        plsData.put("notesPulsar", notesListPulsar); // note relative al singolo ammasso
        plsData.put("biblioRefPulsar", bibRefListPulsar); // riferimenti relativi al singolo ammasso
        plsData.put("imageInfo", imagesList); // info relative alle immagini
        plsData.put("notesImage", notesListImage); // note relative alle immagini
        plsData.put("biblioRefImage", bibRefListImage);  // riferimenti bibliografici relativi alle immagini
        plsData.put("refAuthor", refAuthorList); // corrispondenze fra riferimenti e autori
        plsData.put("tagPulsar", tagPulsarList);  // elenco tag associati all'ammasso
        plsData.put("tagImage", tagImageList); // elenco tag associati alle immagini
        freemarkerDo(plsData, "pulsar.ftl");

    }

    public void createStarInfoList(String starId) throws Exception {
        Map<String, Object> starData = new HashMap<>(15);
        Map<String, Object> attInfo;
        Map<String, Object> notesInfo;
        Map<String, Object> bibRefInfo;
        Map<String, Object> notesInfoStar;
        Map<String, Object> bibRefInfoStar;
        Map<String, Object> imageInfo;
        Map<String, Object> tagImage;
        Map<String, Object> notesInfoImage;
        Map<String, Object> bibRefInfoImage;
        Map<String, Object> tagStar;

        List<Map> attList;
        List<Map> notesList = new ArrayList<>(0);
        List<Map> bibRefList = new ArrayList<>(0);
        List<Map> notesListStar;
        List<Map> bibRefListStar;
        List<Map> imagesList;
        List<Map> tagImageList = new ArrayList<>(0);
        List<Map> notesListImage = new ArrayList<>(0);
        List<Map> bibRefListImage = new ArrayList<>(0);
        List<Map> tagStarList;
        int papId, j;

        Star star = vogc_.selectStar(starId);
        starData.put("Id", starId);
        starData.put("gclusterId", star.getGclusterId());

        StarHasAttribute attributes[] = vogc_.selectStarHasAttByStarId(starId);
        attList = new ArrayList<>(attributes.length);
        for (StarHasAttribute att : attributes) {
            StarAttribute starAtt = vogc_.selectStarAttribute(att.getStarAttId());

            attInfo = new HashMap<>(15);
            attInfo.put("attributeId", starAtt.getId());
            attInfo.put("attributeName", starAtt.getName());
            if (starAtt.getDescription() == null) {
                attInfo.put("attributeDesc", "");
            } else {
                attInfo.put("attributeDesc", starAtt.getDescription());
            }
            attInfo.put("attributeDatatype", starAtt.getDatatype());
            if (starAtt.getUcd() == null) {
                attInfo.put("attributeUcd", "");
            } else {
                attInfo.put("attributeUcd", starAtt.getUcd());
            }

            Source s = vogc_.selectSource(att.getSourceId());
            attInfo.put("sourceId", s.getId());
            attInfo.put("sourceType", s.getType());
            if (s.getType() == 2) {  // se src è un utente recupero nome e cognome
                User usr = vogc_.selectUser(s.getId());
                String nameSurname = usr.getName() + " " + usr.getSurname();
                attInfo.put("userName", validateString(nameSurname));
            } else {
                attInfo.put("userName", "");
            }
            attInfo.put("value", att.getValue());
            attInfo.put("date", att.getDate());
            if (att.isFirst() == true) {
                attInfo.put("isFirst", "1");
            } else {
                attInfo.put("isFirst", "0");
            }
            if (starAtt.isPrimaryAtt() == true) {
                attInfo.put("isPrimary", "1");
            } else {
                attInfo.put("isPrimary", "0");
            }
            attInfo.put("starHasAttId", att.getId());
            attList.add(attInfo);

            BiblioNotes b[] = vogc_.selectBiblioNotesByStarAttributeId(att.getId());
            notesList = new ArrayList<>(b.length);
            bibRefList = new ArrayList<>(b.length);
            for (BiblioNotes bnote : b) {
                User usr = vogc_.selectUser(bnote.getUserId());
                String nameSurname = usr.getName() + " " + usr.getSurname();
                if (bnote.getType() == 1) {

                    Notes note = vogc_.selectNotes(bnote.getId());
                    notesInfo = new HashMap<>(10);
                    notesInfo.put("noteId", note.getId());
                    notesInfo.put("starHasAttId", note.getStarAttributeId());
                    notesInfo.put("title", note.getTitle());
                    notesInfo.put("desc", note.getDescription());
                    notesInfo.put("type", note.getNoteType());
                    notesInfo.put("userId", note.getUserId());
                    notesInfo.put("userName", validateString(nameSurname));
                    notesInfo.put("date", note.getDate());
                    notesList.add(notesInfo);

                } else if (bnote.getType() == 2) {
                    BiblioRef bib = vogc_.selectBiblioRef(bnote.getId());
                    papId = vogc_.selectBiblioRefPaperId(bnote.getId());

                    bibRefInfo = new HashMap<>(15);
                    bibRefInfo.put("bibRefId", bib.getId());
                    bibRefInfo.put("starHasAttId", bib.getStarAttributeId());
                    bibRefInfo.put("paperId", papId);
                    bibRefInfo.put("paperName", vogc_.selectPaper(papId).getName());
                    bibRefInfo.put("paperUri", vogc_.selectPaper(papId).getUri());
                    bibRefInfo.put("userId", bib.getUserId());
                    bibRefInfo.put("userName", validateString(nameSurname));
                    bibRefInfo.put("title", bib.getTitle());
                    if (bib.getDescription() == null) {
                        bibRefInfo.put("desc", "");
                    } else {
                        bibRefInfo.put("desc", bib.getDescription());
                    }
                    if (bib.getUri() == null) {
                        bibRefInfo.put("uri", "");
                    } else {
                        bibRefInfo.put("uri", bib.getUri());
                    }
                    bibRefInfo.put("year", bib.getYear());
                    bibRefInfo.put("date", bib.getDate());
                    bibRefList.add(bibRefInfo);
                }
            }
        }
        BiblioNotes bn[] = vogc_.selectBiblioNotesByObjectId(starId);
        notesListStar = new ArrayList<>(bn.length);
        bibRefListStar = new ArrayList<>(bn.length);
        for (BiblioNotes b : bn) {
            User usr = vogc_.selectUser(b.getUserId());
            String nameSurname = usr.getName() + " " + usr.getSurname();
            if (b.getType() == 1) {
                Notes note = vogc_.selectNotes(b.getId());
                notesInfoStar = new HashMap<>(10);
                notesInfoStar.put("noteId", note.getId());
                notesInfoStar.put("starId", note.getObjectId());
                notesInfoStar.put("title", note.getTitle());
                notesInfoStar.put("desc", note.getDescription());
                notesInfoStar.put("type", note.getNoteType());
                notesInfoStar.put("userId", note.getUserId());
                notesInfoStar.put("userName", validateString(nameSurname));
                notesInfoStar.put("date", note.getDate());
                notesListStar.add(notesInfoStar);
            } else if (b.getType() == 2) {
                BiblioRef bib = vogc_.selectBiblioRef(b.getId());
                papId = vogc_.selectBiblioRefPaperId(b.getId());

                bibRefInfoStar = new HashMap<>(15);
                bibRefInfoStar.put("bibRefId", bib.getId());
                bibRefInfoStar.put("starId", bib.getObjectId());
                bibRefInfoStar.put("paperId", papId);
                bibRefInfoStar.put("paperName", vogc_.selectPaper(papId).getName());
                bibRefInfoStar.put("paperUri", vogc_.selectPaper(papId).getUri());
                bibRefInfoStar.put("userId", bib.getUserId());
                bibRefInfoStar.put("userName", validateString(nameSurname));
                bibRefInfoStar.put("title", bib.getTitle());
                if (bib.getDescription() == null) {
                    bibRefInfoStar.put("desc", "");
                } else {
                    bibRefInfoStar.put("desc", bib.getDescription());
                }
                if (bib.getUri() == null) {
                    bibRefInfoStar.put("uri", "");
                } else {
                    bibRefInfoStar.put("uri", bib.getUri());
                }
                bibRefInfoStar.put("year", bib.getYear());
                bibRefInfoStar.put("date", bib.getDate());
                bibRefListStar.add(bibRefInfoStar);
            }
        }
        Image images[] = vogc_.selectImageByObjectId(starId);
        imagesList = new ArrayList<>(images.length);
        for (Image img : images) {
            User usr = vogc_.selectUser(img.getUserId());
            String nameSurname = usr.getName() + " " + usr.getSurname();
            imageInfo = new HashMap<>(15);
            imageInfo.put("imageId", img.getId());
            imageInfo.put("userId", img.getUserId());
            imageInfo.put("userName", validateString(nameSurname));
            if (img.getName() == null) {
                imageInfo.put("name", "");
            } else {
                imageInfo.put("name", img.getName());
            }
            if (img.getDescription() == null) {
                imageInfo.put("desc", "");
            } else {
                imageInfo.put("desc", img.getDescription());
            }
            if (img.getScale() == null) {
                imageInfo.put("scale", "");
            } else {
                imageInfo.put("scale", img.getScale());
            }
            if (img.getFormat() == null) {
                imageInfo.put("format", "");
            } else {
                imageInfo.put("format", img.getFormat());
            }
            imageInfo.put("dim", img.getDimension());
            imageInfo.put("uri", img.getUri());
            imageInfo.put("date", img.getDate());
            imagesList.add(imageInfo);

            int tagsInImage[] = vogc_.selectTagsInImage(img.getId());
            tagImageList = new ArrayList<>(tagsInImage.length);
            for (int k = 0; k < tagsInImage.length; k++) {
                usr = vogc_.selectUser(vogc_.selectUserByTag(tagsInImage[k]));
                nameSurname = usr.getName() + " " + usr.getSurname();
                Tag tag = vogc_.selectTag(tagsInImage[k]);
                tagImage = new HashMap<>(5);
                tagImage.put("imageId", img.getId());
                tagImage.put("tagId", tag.getId());
                tagImage.put("userId", usr.getId());
                tagImage.put("userName", validateString(nameSurname));
                tagImage.put("tagName", tag.getName());

                tagImageList.add(tagImage);
            }
            BiblioNotes bnImages[] = vogc_.selectBiblioNotesByImageId(img.getId());
            notesListImage = new ArrayList<>(bnImages.length);
            bibRefListImage = new ArrayList<>(bnImages.length);
            for (BiblioNotes b : bnImages) {
                usr = vogc_.selectUser(b.getUserId());
                nameSurname = usr.getName() + " " + usr.getSurname();
                if (b.getType() == 1) {
                    Notes note = vogc_.selectNotes(b.getId());
                    notesInfoImage = new HashMap<>(10);
                    notesInfoImage.put("noteId", note.getId());
                    notesInfoImage.put("imageId", note.getImageId());
                    notesInfoImage.put("title", note.getTitle());
                    notesInfoImage.put("desc", note.getDescription());
                    notesInfoImage.put("type", note.getNoteType());
                    notesInfoImage.put("userId", note.getUserId());
                    notesInfoImage.put("userName", validateString(nameSurname));
                    notesInfoImage.put("date", note.getDate());
                    notesListImage.add(notesInfoImage);

                } else if (b.getType() == 2) {
                    BiblioRef bib = vogc_.selectBiblioRef(b.getId());
                    papId = vogc_.selectBiblioRefPaperId(b.getId());

                    bibRefInfoImage = new HashMap<>(15);
                    bibRefInfoImage.put("bibRefId", bib.getId());
                    bibRefInfoImage.put("imageId", bib.getImageId());
                    bibRefInfoImage.put("paperId", papId);
                    bibRefInfoImage.put("paperName", vogc_.selectPaper(papId).getName());
                    bibRefInfoImage.put("paperUri", vogc_.selectPaper(papId).getUri());
                    bibRefInfoImage.put("userId", bib.getUserId());
                    bibRefInfoImage.put("userName", validateString(nameSurname));
                    bibRefInfoImage.put("title", bib.getTitle());
                    if (bib.getDescription() == null) {
                        bibRefInfoImage.put("desc", "");
                    } else {
                        bibRefInfoImage.put("desc", bib.getDescription());
                    }
                    if (bib.getUri() == null) {
                        bibRefInfoImage.put("uri", "");
                    } else {
                        bibRefInfoImage.put("uri", bib.getUri());
                    }
                    bibRefInfoImage.put("year", bib.getYear());
                    bibRefInfoImage.put("date", bib.getDate());
                    bibRefListImage.add(bibRefInfoImage);
                }
            }
        }
        j = 0;
        int idSize = bibRefList.size() + bibRefListStar.size() + bibRefListImage.size();
        List<Object> idRef = new ArrayList<>(idSize);
        while (j < bibRefList.size()) {  // recupero gli id dei riferimenti relativi agli attributi
            idRef.add(bibRefList.get(j).get("bibRefId"));
            j++;
        }
        j = 0;
        while (j < bibRefListStar.size()) { // recupero gli id dei riferimenti relativi alla stella
            idRef.add(bibRefListStar.get(j).get("bibRefId"));
            j++;
        }
        j = 0;

        while (j < bibRefListImage.size()) { // recupero gli id dei riferimenti relativi ad un'immagine
            idRef.add(bibRefListImage.get(j).get("bibRefId"));
            j++;
        }
        Map<String, Object> refAuthor;
        List<Map> refAuthorList = new ArrayList<>(idRef.size() * 5);
        int authorsId[];
        for (j = 0; j < idRef.size(); j++) {
            authorsId = vogc_.selectAuthorsInBiblioRef((Integer) idRef.get(j));  // recupero gli id degli autori
            for (int k = 0; k < authorsId.length; k++) {
                refAuthor = new HashMap<>(5);
                Author a = vogc_.selectAuthor(authorsId[k]);
                refAuthor.put("biblioRefId", idRef.get(j));
                refAuthor.put("authorId", a.getId());
                refAuthor.put("authorName", validateString(a.getName()));
                if (a.getUri() == null) {
                    refAuthor.put("authorUri", "");
                } else {
                    refAuthor.put("authorUri", a.getUri());
                }
                refAuthorList.add(refAuthor);
            }
        }
        int tagsId[] = vogc_.selectTagsInVObject(starId);
        tagStarList = new ArrayList<>(tagsId.length);
        for (int k = 0; k < tagsId.length; k++) {
            Tag tag = vogc_.selectTag(tagsId[k]);
            User usr = vogc_.selectUser(vogc_.selectUserByTag(tag.getId()));
            String nameSurname = usr.getName() + " " + usr.getSurname();
            tagStar = new HashMap<>(5);
            tagStar.put("tagId", tag.getId());
            tagStar.put("userId", vogc_.selectUserByTag(tag.getId()));
            tagStar.put("userName", validateString(nameSurname));
            tagStar.put("tagName", tag.getName());
            tagStarList.add(tagStar);
        }
        starData.put("attributeInfo", attList);  // descrizione degli attibuti
        starData.put("notes", notesList);  // note relative agli attributi
        starData.put("biblioRef", bibRefList);  // riferimenti relativi agli attributi
        starData.put("notesStar", notesListStar); // note relative alla stella
        starData.put("biblioRefStar", bibRefListStar); // riferimenti relativi alla stella
        starData.put("imageInfo", imagesList); // info relative alle immagini
        starData.put("notesImage", notesListImage); // note relative alle immagini
        starData.put("biblioRefImage", bibRefListImage);  // riferimenti bibliografici relativi alle immagini
        starData.put("refAuthor", refAuthorList); // corrispondenze fra riferimenti e autori
        starData.put("tagStar", tagStarList);  // elenco tag associati all'ammasso
        starData.put("tagImage", tagImageList); // elenco tag associati alle immagini
        freemarkerDo(starData, "star.ftl");
    }

    public void createClustersList() throws Exception {

        Map<String, Object> clustersInfo = new HashMap<>(5);
        Map<String, Object> galClusters;
        Map<String, Object> extraClusters;
        List<Map> galList;
        List<Map> extraList;

        Gcluster galactic[] = vogc_.selectGclustersByType(1);
        galList = new ArrayList<>(galactic.length);
        for (Gcluster g : galactic) {
            if (g.getId().compareTo("0000000000") != 0) {
                galClusters = new HashMap<>(1);
                galClusters.put("clusterId", g.getId());
                galList.add(galClusters);
            }
        }
        Gcluster extragal[] = vogc_.selectGclustersByType(2);
        extraList = new ArrayList<>(extragal.length);
        for (Gcluster ex : extragal) {
            extraClusters = new HashMap<>(1);
            extraClusters.put("clusterId", ex.getId());
            extraList.add(extraClusters);
        }
        clustersInfo.put("nrowGal", galactic.length - 1);
        clustersInfo.put("GalacticList", galList);
        clustersInfo.put("nrowEx", extragal.length);
        clustersInfo.put("ExtragalacticList", extraList);

        freemarkerDo(clustersInfo, "clustersList.ftl");

    }

    public void createParameterSearchResults(int aInt, String value, String operator) throws Exception {

        String clusters[] = vogc_.selectClusterParameterResult(aInt, value, operator);
        Map<String, Object> searchInfo, galClusters;
        List<Map> galList = new ArrayList<>(clusters.length);
//        List<Map> tagList = new ArrayList<>(0);

        searchInfo = new HashMap<>(5);
        searchInfo.put("stringSearched", aInt);
        for (String cluster : clusters) {
            galClusters = new HashMap<>(1);
            galClusters.put("clusterId", cluster);
            galList.add(galClusters);

//            tagList.add(galClusters);
        }
        searchInfo.put("nrowGal", 1);
        searchInfo.put("GalacticList", galList);

        freemarkerDo(searchInfo, "GclusterParameterList.ftl");

    }

    public void createPartialResult(String value) throws Exception {

        String object[] = vogc_.selectPartialResult(value);
        Map<String, Object> searchInfo, galClusters;

        searchInfo = new HashMap<>(5);
        searchInfo.put("stringSearched", value);
        List<Map> galList = new ArrayList<>(object.length);
//        List<Map> tagList = new ArrayList<>(object.length);
        for (String object1 : object) {
            galClusters = new HashMap<>(1);
            galClusters.put("clusterId", object1);
            galList.add(galClusters);
//            tagList.add(galClusters);
        }
        searchInfo.put("nrowGal", 1);
        searchInfo.put("GalacticList", galList);
        freemarkerDo(searchInfo, "GclusterParameterList.ftl");
    }

    public void createSearchResults(String toSearch) throws Exception {

        int tags[] = vogc_.selectTagContainingAString(toSearch);
        vogc_.selectVObjectSameTag(i);
        int images[];
        String vobjects[];
        VObject vobject;
        Image img;
        Map<String, Object> imageInfo, vobjectInfo, searchInfo, tagInfo;
        List<Map> imagesList = new ArrayList<>(0);
        List<Map> vobjectsList = new ArrayList<>(0);
        List<Map> tagList = new ArrayList<>(tags.length);
        List<Integer> idTemp;
//        List<String> idTemp2 = new ArrayList<>(0);
        boolean flag;

        searchInfo = new HashMap<>(5);
        searchInfo.put("stringSearched", toSearch);

        for (int aTag : tags) {

            tagInfo = new HashMap<>(1);
            tagInfo.put("name", vogc_.selectTagName(aTag));
            images = vogc_.selectImageSameTag(aTag);
            imagesList = new ArrayList<>(images.length);
            idTemp = new ArrayList<>(images.length);
            for (int aImageId : images) {

                flag = false;

                for (Integer idTemp1 : idTemp) {
                    if (idTemp1.equals(aImageId)) {
                        flag = true;
                    }
                }
                if (flag == false) {
                    img = vogc_.selectImage(aImageId);
                    imageInfo = new HashMap<>(5);
                    imageInfo.put("imageId", img.getId());
                    imageInfo.put("imageName", validateString(img.getName()));
                    imageInfo.put("imageUri", validateString(img.getUri()));
                    imageInfo.put("objectId", img.getObjectId());
                    idTemp.add(aImageId);
                    imagesList.add(imageInfo);
                }
            }

            vobjects = vogc_.selectVObjectSameTag(aTag);
            vobjectsList = new ArrayList<>(vobjects.length);
            for (String aVobjectId : vobjects) {
                flag = false;

                for (Integer idTemp1 : idTemp) {
                    if (idTemp1.equals(Integer.parseInt(
                            aVobjectId))) {
                        flag = true;
                    }
                }
                if (flag == false) {
                    vobject = vogc_.selectVObject(aVobjectId);
                    vobjectInfo = new HashMap<>(2);
                    vobjectInfo.put("vobjectId", vobject.getId());
                    vobjectInfo.put("type", vobject.getType());
//                    idTemp2.add(aVobjectId);
                    vobjectsList.add(vobjectInfo);
                }
            }

            tagList.add(tagInfo);
        }
        if (imagesList.isEmpty() && vobjectsList.isEmpty()) {
            generateErrorReport(Messages.getError(Messages.NO_MATCH_FOUND));
        } else {
            searchInfo.put("tagList", tagList);
            searchInfo.put("imagesList", imagesList);
            searchInfo.put("vobjectsList", vobjectsList);
            freemarkerDo(searchInfo, "searchResults.ftl");
        }
    }

    public void createTagList() throws Exception {
        String[] tags = vogc_.selectAllTag();
        Map<String, Object> aTag;
        Map<String, Object> allTags = new HashMap<>(1);
        List<Map> tagList = new ArrayList<>(tags.length);
        for (String t : tags) {
            aTag = new HashMap<>(1);
            aTag.put("name", validateString(t));
            tagList.add(aTag);
        }
        allTags.put("tagsList", tagList);
        freemarkerDo(allTags, "tags.ftl");
        /*
         * if(tags.length == 0)
         * generateErrorReport(Messages.getError(Messages.));
         */
    }

    public void createCatalogueInfo(String catalogId) throws Exception {
        Catalogue c = vogc_.selectCatalogue(catalogId);
        Map<String, Object> catInfo = new HashMap<>(10);

        Map<String, Object> authorInfo;
        Author author;

        int[] authorsId = vogc_.selectAuthorsInCatalogue(catalogId);
        List<Map> authorsList = new ArrayList<>(authorsId.length);
        catInfo.put("catId", validateString(catalogId));
        catInfo.put("title", validateString(c.getTitle()));
        if (c.getDescription() == null) {
            catInfo.put("desc", "");
        } else {
            catInfo.put("desc", validateString(c.getDescription()));
        }
        catInfo.put("year", validateString(c.getYear()));
        catInfo.put("uri", validateString(c.getUri()));
        catInfo.put("date", c.getDate());
        for (int a : authorsId) {
            author = vogc_.selectAuthor(a);
            authorInfo = new HashMap<>(5);
            authorInfo.put("authorId", author.getId());
            authorInfo.put("name", validateString(author.getName()));
            if (author.getUri() == null) {
                authorInfo.put("uri", "");
            } else {
                authorInfo.put("uri", validateString(author.getUri()));
            }
            authorsList.add(authorInfo);
        }
        catInfo.put("authorsList", authorsList);
        freemarkerDo(catInfo, "catalogue.ftl");

    }
// type = 1 se è un attributo di un ammasso globulare, 2 att pulsar, 3 att star

    public void createParameterHistory(String attributeName, String objectId) throws Exception {
        Map<String, Object> attributeInfo = new HashMap<>(15);
        Map<String, Object> valueInfo;
        int objType = vogc_.selectVObjectType(objectId);
        List<Map> attributeList = new ArrayList<>(1);
        switch (objType) {
            case 1: { // gcluster attribute
                // GclusterHasAttribute[] values = vogc_.selectGcHasAttByGclusterId(objectId);

                int attributeId = vogc_.selectGcAttributeIdByName(attributeName);
                GclusterHasAttribute[] values = vogc_.selectGcHasAttByGclusterIdandObjectId(objectId, attributeId);

                GcAttribute att = vogc_.selectGcAttribute(attributeId);
                attributeInfo.put("attName", attributeName);
                attributeInfo.put("attType", att.getType()); //posiz, param, fotom, dinam o gener
                attributeInfo.put("objectId", objectId);
                attributeInfo.put("objectType", objType); // cluster, pulsar o star
                attributeInfo.put("desc", att.getDescription());
                if (att.getUcd() == null) {
                    attributeInfo.put("ucd", "");
                } else {
                    attributeInfo.put("ucd", att.getUcd());
                }
                attributeInfo.put("dataType", att.getDatatype());
                if (att.isPrimaryAtt() == true) {
                    attributeInfo.put("primaryAtt", "1");
                } else {
                    attributeInfo.put("primaryAtt", "0");
                }
                for (GclusterHasAttribute aValue : values) {
                    if (aValue.getGcAttId() == attributeId) {
                        valueInfo = new HashMap<>(10);
                        Source src = vogc_.selectSource(aValue.getSourceId());

                        valueInfo.put("value", validateString(aValue.getValue()));
                        if (aValue.isFirst() == true) {
                            valueInfo.put("first", "1");
                        } else {
                            valueInfo.put("first", "0");
                        }
                        valueInfo.put("date", aValue.getDate());
                        valueInfo.put("sourceId", validateString(aValue.getSourceId()));
                        valueInfo.put("sourceType", src.getType());
                        if (src.getType() == 2) {  // se src è un utente recupero nome e cognome
                            User usr = vogc_.selectUser(src.getId());
                            String nameSurname = usr.getName() + " " + usr.getSurname();
                            valueInfo.put("userName", validateString(nameSurname));
                        } else {
                            valueInfo.put("userName", "");
                        }
                        attributeList.add(valueInfo);
                    }
                }
                attributeInfo.put("valuesList", attributeList);
                break;
            }
            case 2: { // pulsar attribute
                int attributeId = vogc_.selectPlsAttributeIdByName(attributeName);
                PulsarHasAttribute[] values = vogc_.selectPlsHasAttByPulsarIdandObjectId(objectId, attributeId);
                PlsAttribute att = vogc_.selectPlsAttribute(attributeId);
                attributeInfo.put("attName", attributeName);
                attributeInfo.put("attType", "");
                attributeInfo.put("objectId", objectId);
                attributeInfo.put("objectType", objType); // cluster, pulsar o star
                attributeInfo.put("desc", att.getDescription());
                if (att.getUcd() == null) {
                    attributeInfo.put("ucd", "");
                } else {
                    attributeInfo.put("ucd", att.getUcd());
                }
                attributeInfo.put("dataType", att.getDatatype());
                if (att.isPrimaryAtt() == true) {
                    attributeInfo.put("primaryAtt", "1");
                } else {
                    attributeInfo.put("primaryAtt", "0");
                }
                for (PulsarHasAttribute aValue : values) {
                    if (aValue.getPlsAttId() == attributeId) {
                        valueInfo = new HashMap<>(10);
                        Source src = vogc_.selectSource(aValue.getSourceId());
                        valueInfo.put("value", validateString(aValue.getValue()));
                        if (aValue.isFirst() == true) {
                            valueInfo.put("first", "1");
                        } else {
                            valueInfo.put("first", "0");
                        }
                        valueInfo.put("date", aValue.getDate());
                        valueInfo.put("sourceId", validateString(aValue.getSourceId()));
                        valueInfo.put("sourceType", src.getType());
                        if (src.getType() == 2) {  // se src è un utente recupero nome e cognome
                            User usr = vogc_.selectUser(src.getId());
                            String nameSurname = usr.getName() + " " + usr.getSurname();
                            valueInfo.put("userName", validateString(nameSurname));
                        } else {
                            valueInfo.put("userName", "");
                        }

                        attributeList.add(valueInfo);
                    }
                }
                attributeInfo.put("valuesList", attributeList);
                break;
            }
            case 3: { // star attribute
                int attributeId = vogc_.selectStarAttributeIdByName(attributeName);
                StarHasAttribute[] values = vogc_.selectStarHasAttByStarIdandObjectId(objectId, attributeId);

                StarAttribute att = vogc_.selectStarAttribute(attributeId);
                attributeInfo.put("attName", attributeName);
                attributeInfo.put("attType", "");
                attributeInfo.put("objectId", objectId);
                attributeInfo.put("objectType", objType); // cluster, pulsar o star
                attributeInfo.put("desc", att.getDescription());
                if (att.getUcd() == null) {
                    attributeInfo.put("ucd", "");
                } else {
                    attributeInfo.put("ucd", att.getUcd());
                }
                attributeInfo.put("dataType", att.getDatatype());
                if (att.isPrimaryAtt() == true) {
                    attributeInfo.put("primaryAtt", "1");
                } else {
                    attributeInfo.put("primaryAtt", "0");
                }
                for (StarHasAttribute aValue : values) {
                    if (aValue.getStarAttId() == attributeId) {
                        valueInfo = new HashMap<>(10);
                        Source src = vogc_.selectSource(aValue.getSourceId());
                        valueInfo.put("value", validateString(aValue.getValue()));
                        if (aValue.isFirst() == true) {
                            valueInfo.put("first", "1");
                        } else {
                            valueInfo.put("first", "0");
                        }
                        valueInfo.put("date", aValue.getDate());
                        valueInfo.put("sourceId", validateString(aValue.getSourceId()));
                        valueInfo.put("sourceType", src.getType());
                        if (src.getType() == 2) {  // se src è un utente recupero nome e cognome
                            User usr = vogc_.selectUser(src.getId());
                            String nameSurname = usr.getName() + " " + usr.getSurname();
                            valueInfo.put("userName", validateString(nameSurname));
                        } else {
                            valueInfo.put("userName", "");
                        }

                        attributeList.add(valueInfo);
                    }
                }
                attributeInfo.put("valuesList", attributeList);
                break;
            }

        }
        freemarkerDo(attributeInfo, "parameterHistory.ftl");
    }

    public void createUserInformation(String userId) throws Exception {
        Map<String, Object> userInfo = new HashMap<>(5);
        User usr = vogc_.selectUser(userId);
        userInfo.put("userId", validateString(usr.getId()));
        userInfo.put("name", validateString(usr.getName()));
        userInfo.put("surname", validateString(usr.getSurname()));
        if (usr.getAffiliation() == null) {
            userInfo.put("motivations", "");
        } else {
            userInfo.put("motivations", validateString(usr.getAffiliation()));
        }
        freemarkerDo(userInfo, "user.ftl");
    }

    public void createAttributeList(int type) throws Exception { // type = 1 gcAttribute, 2 plsAttribute, 3 starAttribute
        Map<String, Object> allAttributes = new HashMap<>(1);
        Map<String, Object> attInfo;
        List<Map> attList = new ArrayList<>(0);
        switch (type) {
            case 1: {
                GcAttribute[] attributes = vogc_.selectAllGcAttribute();
                attList = new ArrayList<>(attributes.length);
                for (GcAttribute att : attributes) {
                    attInfo = new HashMap<>(2);
                    if (att.getName().compareTo("EMPTY") != 0) {
                        attInfo.put("name", att.getName());
                        attInfo.put("datatype", att.getDatatype());
                        attList.add(attInfo);
                    }
                }
                break;
            }
            case 2: {
                PlsAttribute[] attributes = vogc_.selectAllPlsAttribute();
                attList = new ArrayList<>(attributes.length);
                for (PlsAttribute att : attributes) {
                    attInfo = new HashMap<>(2);
                    if (att.getName().compareTo("EMPTY") != 0) {
                        attInfo.put("name", att.getName());
                        attInfo.put("datatype", att.getDatatype());
                        attList.add(attInfo);
                    }
                }
                break;
            }
            case 3: {
                StarAttribute[] attributes = vogc_.selectAllStarAttribute();
                attList = new ArrayList<>(attributes.length);
                for (StarAttribute att : attributes) {
                    attInfo = new HashMap<>(2);
                    if (att.getName().compareTo("EMPTY") != 0) {
                        attInfo.put("name", att.getName());
                        attInfo.put("datatype", att.getDatatype());
                        attList.add(attInfo);
                    }
                }
                break;
            }

        }
        allAttributes.put("attributes", attList);
        freemarkerDo(allAttributes, "attributes.ftl");
    }

    public void createParameterList(int i) throws Exception {
        Map<String, Object> attributeInfo = new HashMap<>(1);
        Map<String, Object> valueInfo;

        List<Map> valueList = new ArrayList<>(0);
        if (i == 1) {
            GclusterHasAttribute[] attribute = vogc_.selectPositionalParameter();
            valueList = new ArrayList<>(attribute.length);
            for (GclusterHasAttribute att : attribute) {
                valueInfo = new HashMap<>(10);

                if (att.getGclusterId().compareTo("EMPTY") != 0) {
                    valueInfo.put("att", validateString(vogc_.selectGcAttributeName(att.getGcAttId())));
                    valueInfo.put("id", att.getId());
                    valueInfo.put("gclusterId", att.getGclusterId());
                    valueInfo.put("gcAttributeId", att.getGcAttId());
                    valueInfo.put("sourceId", att.getSourceId());
                    valueInfo.put("value", att.getValue());

                    valueInfo.put("date", att.getDate());
                    valueList.add(valueInfo);
                }

            }

        } else if (i == 2) {
            GclusterHasAttribute[] attribute = vogc_.selectPositionalParameter();
            valueList = new ArrayList<>(attribute.length);
            for (GclusterHasAttribute att : attribute) {
                valueInfo = new HashMap<>(10);
                if (att.getGclusterId().compareTo("EMPTY") != 0) {
                    valueInfo.put("att", validateString(vogc_.selectGcAttributeName(att.getGcAttId())));
                    valueInfo.put("id", att.getId());
                    valueInfo.put("gclusterId", att.getGclusterId());
                    valueInfo.put("gcAttributeId", att.getGcAttId());
                    valueInfo.put("sourceId", att.getSourceId());
                    valueInfo.put("value", att.getValue());

                    valueInfo.put("date", att.getDate());
                    valueList.add(valueInfo);
                }

            }

        } else if (i == 3) {
            GclusterHasAttribute[] attribute = vogc_.selectStructuralParameter();
            valueList = new ArrayList<>(attribute.length);
            for (GclusterHasAttribute att : attribute) {
                valueInfo = new HashMap<>(10);
                if (att.getGclusterId().compareTo("EMPTY") != 0) {
                    valueInfo.put("att", validateString(vogc_.selectGcAttributeName(att.getGcAttId())));
                    valueInfo.put("id", att.getId());
                    valueInfo.put("gclusterId", att.getGclusterId());
                    valueInfo.put("gcAttributeId", att.getGcAttId());
                    valueInfo.put("sourceId", att.getSourceId());
                    valueInfo.put("value", att.getValue());

                    valueInfo.put("date", att.getDate());
                    valueList.add(valueInfo);
                }

            }

        }
        attributeInfo.put("attributes", valueList);

        freemarkerDo(attributeInfo, "HasAttribute.ftl");
    }

    public void createInfoForBiblioNotes(String objectId) throws Exception {
        Map<String, Object> infoForNotes = new HashMap<>(10);
        Map<String, Object> attInfo, authorInfo, paperInfo, imgInfo;
        List<Map> attributeList = new ArrayList<>(0);
        List<Map> authorList;
        List<Map> paperList;
        List<Map> imageList;

        int objType = vogc_.selectVObjectType(objectId);
        infoForNotes.put("Id", objectId);
        if (objType == 1) {  // GLOBULAR CLUSTER
            Gcluster cluster = vogc_.selectGcluster(objectId);
            if (cluster.getName() != null) {
                infoForNotes.put("Name", cluster.getName());
            }
            infoForNotes.put("type", cluster.getType());
            GclusterHasAttribute att[] = vogc_.selectGcHasAttByGclusterId(objectId);
            attributeList = new ArrayList<>(att.length);
            for (GclusterHasAttribute a : att) {
                attInfo = new HashMap<>(10);
                attInfo.put("objHasAttId", a.getId());
                GcAttribute attr = vogc_.selectGcAttribute(a.getGcAttId());
                attInfo.put("attName", attr.getName());
                if (attr.getDescription() == null) {
                    attInfo.put("attDesc", "");
                } else {
                    attInfo.put("attDesc", validateString(attr.getDescription()));
                }
                Source s = vogc_.selectSource(a.getSourceId());
                attInfo.put("sourceId", validateString(s.getId()));
                attInfo.put("sourceType", s.getType());
                if (s.getType() == 2) {  // se src è un utente recupero nome e cognome
                    User usr = vogc_.selectUser(s.getId());
                    String nameSurname = usr.getName() + " " + usr.getSurname();
                    attInfo.put("userName", validateString(nameSurname));
                } else {
                    attInfo.put("userName", "");
                }

                attInfo.put("value", validateString(a.getValue()));
                attInfo.put("date", a.getDate());
                attributeList.add(attInfo);
            }

        } else if (objType == 2) {  //PULSAR
            infoForNotes.put("Name", "");
            infoForNotes.put("type", "");
            PulsarHasAttribute att[] = vogc_.selectPlsHasAttByPulsarId(objectId);
            attributeList = new ArrayList<>(att.length);
            for (PulsarHasAttribute pls : att) {
                attInfo = new HashMap<>(10);
                PlsAttribute attr = vogc_.selectPlsAttribute(pls.getPlsAttId());
                attInfo.put("attributeId", attr.getId());
                attInfo.put("attributeName", attr.getName());
                if (attr.getDescription() == null) {
                    attInfo.put("attributeDesc", "");
                } else {
                    attInfo.put("attributeDesc", validateString(attr.getDescription()));
                }
                Source s = vogc_.selectSource(pls.getSourceId());
                attInfo.put("sourceId", validateString(s.getId()));
                attInfo.put("sourceType", s.getType());
                if (s.getType() == 2) {  // se src è un utente recupero nome e cognome
                    User usr = vogc_.selectUser(s.getId());
                    String nameSurname = usr.getName() + " " + usr.getSurname();
                    attInfo.put("userName", validateString(nameSurname));
                } else {
                    attInfo.put("userName", "");
                }

                attInfo.put("value", validateString(pls.getValue()));
                attInfo.put("date", pls.getDate());
                attributeList.add(attInfo);
            }
        } else if (objType == 3) {   // STAR
            infoForNotes.put("Name", "");
            infoForNotes.put("type", "");
            StarHasAttribute att[] = vogc_.selectStarHasAttByStarId(objectId);
            attributeList = new ArrayList<>(att.length);
            for (StarHasAttribute st : att) {
                attInfo = new HashMap<>(10);
                PlsAttribute attr = vogc_.selectPlsAttribute(st.getStarAttId());
                attInfo.put("attributeId", attr.getId());
                attInfo.put("attributeName", attr.getName());
                if (attr.getDescription() == null) {
                    attInfo.put("attributeDesc", "");
                } else {
                    attInfo.put("attributeDesc", validateString(attr.getDescription()));
                }
                Source s = vogc_.selectSource(st.getSourceId());
                attInfo.put("sourceId", validateString(s.getId()));
                attInfo.put("sourceType", s.getType());
                if (s.getType() == 2) {  // se src è un utente recupero nome e cognome
                    User usr = vogc_.selectUser(s.getId());
                    String nameSurname = usr.getName() + " " + usr.getSurname();
                    attInfo.put("userName", validateString(nameSurname));
                } else {
                    attInfo.put("userName", "");
                }
                attInfo.put("value", validateString(st.getValue()));
                attInfo.put("date", st.getDate());
                attributeList.add(attInfo);
            }
        }
        Author authors[] = vogc_.selectAllAuthors();
        authorList = new ArrayList<>(authors.length);
        for (Author a : authors) {
            authorInfo = new HashMap<>(2);
            authorInfo.put("id", a.getId());
            authorInfo.put("name", a.getName());
            authorList.add(authorInfo);
        }
        Paper papers[] = vogc_.selectAllPapers();
        paperList = new ArrayList<>(papers.length);
        for (Paper p : papers) {
            paperInfo = new HashMap<>(2);
            paperInfo.put("id", p.getId());
            paperInfo.put("name", p.getName());
            paperList.add(paperInfo);
        }
        Image[] images = vogc_.selectImageByObjectId(objectId);
        imageList = new ArrayList<>(images.length);
        for (Image im : images) {
            imgInfo = new HashMap<>(10);
            imgInfo.put("id", im.getId());
            imgInfo.put("userId", im.getUserId());
            if (im.getName() == null) {
                imgInfo.put("name", "");
            } else {
                imgInfo.put("name", im.getName());
            }
            if (im.getDescription() == null) {
                imgInfo.put("desc", "");
            } else {
                imgInfo.put("desc", im.getDescription());
            }
            if (im.getScale() == null) {
                imgInfo.put("scale", "");
            } else {
                imgInfo.put("scale", im.getScale());
            }
            if (im.getFormat() == null) {
                imgInfo.put("format", "");
            } else {
                imgInfo.put("format", im.getFormat());
            }

            imgInfo.put("dim", validateString(im.getDimension()));
            imgInfo.put("uri", im.getUri());
            imgInfo.put("date", im.getDate());
            imgInfo.put("type", im.getType());
            imageList.add(imgInfo);
        }
        infoForNotes.put("AttributeList", attributeList);
        infoForNotes.put("AuthorList", authorList);
        infoForNotes.put("PaperList", paperList);
        infoForNotes.put("ImageList", imageList);
        freemarkerDo(infoForNotes, "infoForNotes.ftl");

    }

    public void freemarkerDo(Map datamodel, String template) throws IOException, TemplateException {
        Template tpl = cfg_.getTemplate(template);
        tpl.process(datamodel, out_);
    }

    public String validateString(String toValidate) {

        String validated = toValidate;
        if (toValidate.contains("&") == true) {
            validated = toValidate.replace("&", "&amp;");
        }
        if (toValidate.contains("<") == true) {
            validated = toValidate.replace("<", "&lt;");
        }
        if (toValidate.contains(">") == true) {
            validated = toValidate.replace(">", "&gt;");
        }
        if (toValidate.contains("\"") == true) {
            validated = toValidate.replace("\"", "&quot;");
        }
        if (toValidate.contains("'") == true) {
            validated = toValidate.replace("'", "&apos;");
        }  // ×
        if (toValidate.contains("×") == true) {
            validated = toValidate.replace("×", "x");
        }
        return validated;
    }

    public void generateErrorReport(Error error) throws TemplateException, IOException {
        generateErrorReport(error, " ");
    }

    public void generateErrorReport(Error error, String info) throws TemplateException, IOException {
        Map<String, String> dataModel = new HashMap<>(3);
        dataModel.put("errorCode", error.getCode());
        dataModel.put("errorMessage", error.getMessage());
        dataModel.put("info", info);
        freemarkerDo(dataModel, "errorReport.ftl");

    }
}
