package org.dame.vogc.framework;

import org.dame.vogc.framework.webservice.WebConfig;

/**
 *
 *
 * @author EM
 *
 * mod by M
 */
public class ServerConstants {

    private final static String HOME = "c:\\";
    public static String XML_TEMPLATE = HOME + "vogcluster\\Templates\\";
    public static String DIR = HOME + "vogcluster\\directory\\";
//     public static String XML_TEMPLATE = "C:/max/universita/progetti/DAME/SuiteSW/VOGCLUSTERS/deployment/vogcluster/Templates/";
//    public static String DIR ="C:/max/universita/progetti/DAME/SuiteSW/VOGCLUSTERS/deployment/vogcluster/directory/";
    public static final String DBUSER = WebConfig.getVogcUser();
    public static final String DBPWD = WebConfig.getVogcPass();
}
