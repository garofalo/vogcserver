package org.dame.vogc.framework.webservice;

import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.datatypes.StarHasAttribute;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class NewAttributeValueServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/xml");
        out = response.getWriter();
        XMLGenerator gen = new XMLGenerator(out);
        String userId = request.getParameter("userId");
        String objectId = request.getParameter("objectId");
        int attributeId = Integer.parseInt(request.getParameter("attId"));
        String attributeValue = request.getParameter("attValue");
        /*
         * FileWriter wrt = null;
         * wrt = new FileWriter(new
         * File("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.txt"));
         * wrt.write(" userId= "+userId);
         * wrt.write(" objectId= "+objectId);
         * wrt.write(" attId ="+String.valueOf(attributeId).trim());
         * wrt.write(" attvalue= "+attributeValue);
         * wrt.close();
         */

        try {
            if (userId == null || attributeId <= 0 || objectId == null || attributeValue == null) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            }
            IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
            int vobjectType = vogc.selectVObjectType(objectId);
            switch (vobjectType) {
                case 1: {
                    GclusterHasAttribute gcHasAtt = new GclusterHasAttribute(0, objectId, attributeId, userId, attributeValue, false, null);
                    vogc.insertGclusterHasAttribute(gcHasAtt);
                    gen.generateErrorReport(Messages.getError(Messages.NEW_ATTRIBUTE_INSERTED));
                    break;
                }
                case 2: {
                    PulsarHasAttribute plsHasAtt = new PulsarHasAttribute(0, attributeId, objectId, userId, attributeValue, null, false);
                    vogc.insertPulsarHasAttribute(plsHasAtt);
                    gen.generateErrorReport(Messages.getError(Messages.NEW_ATTRIBUTE_INSERTED));
                    break;
                }
                case 3: {
                    StarHasAttribute starHasAtt = new StarHasAttribute(0, attributeId, objectId, userId, attributeValue, null, false);
                    vogc.insertStarHasAttribute(starHasAtt);
                    gen.generateErrorReport(Messages.getError(Messages.NEW_ATTRIBUTE_INSERTED));
                    break;
                }
            }
        } catch (TemplateException | IOException | VogcException | SQLException ex) {
            Messages.sendError(ex, out);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
