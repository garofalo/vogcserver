package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;

/**
 *
 * @author Luca
 */
public class EnablingServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out;
        XMLGenerator gen;
        String[] recipients = {"HARRIS2010"};
        String message;
        /*
         * String userId = (String)request.getParameter("userId");
         * String name = (String)request.getParameter("name");
         * String surname = (String)request.getParameter("surname");
         * String motivations = (String)request.getParameter("motivations");
         */
        String userId = "sabrina_19@alice.it";
        String name = "sabrina";
        String surname = "checola";
        String motivations = "my motivations";

        response.setContentType("text/xml");
        out = response.getWriter();
        gen = new XMLGenerator(out);
        try {
            if (userId == null || name == null || surname == null) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            } else {
                message = "userId= " + userId + "\n"
                        + "name= " + name + "\n"
                        + "surname= " + surname + "\n"
                        + "motivations= " + motivations;

                EMail mail = new EMail(recipients, "New Enabling Request", message, "sabrina_19@alice.it");
                mail.postMail();

                gen.generateErrorReport(Messages.getError(Messages.EMAIL_SENT));
            }
        } catch (TemplateException | IOException ex) {
            Messages.sendError(ex, out);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
