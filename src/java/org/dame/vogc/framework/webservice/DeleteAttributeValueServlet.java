package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.datatypes.BiblioNotes;
import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.datatypes.StarHasAttribute;
import org.dame.vogc.datatypes.VObject;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author Luca
 */
public class DeleteAttributeValueServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml");
        PrintWriter out = response.getWriter();
        XMLGenerator gen;
        int type;
        String userId = request.getParameter("userId");
        String objectId = request.getParameter("objectId");
        String root = "admin";
        int vobjHasAttId = Integer.parseInt(request.getParameter("vobjHasAttId"));
        /*
         * String userId = "smith@gmail.com";
         * String objectId = "NGC 288";
         * int vobjHasAttId = 394;
         */
        BiblioNotes[] biblioGc;

        gen = new XMLGenerator(out);
        try {
            if (userId == null || objectId == null || vobjHasAttId < 0) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            } else {
                IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
                VObject obj = vogc.selectVObject(objectId);
                type = obj.getType();
                List<Integer> idToDel;

                switch (type) {

                    case 1:
                        // globular cluster attribute value
                        a:
                        {
                            GclusterHasAttribute gcHasAttObject = vogc.selectGclusterHasAttribute(vobjHasAttId);
                            if (!gcHasAttObject.getSourceId().equals(userId) && !root.equals(userId)) {

                                gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                break a;
                            }
                            biblioGc = vogc.selectBiblioNotesByGcAttributeId(vobjHasAttId);
                            idToDel = new ArrayList<>(biblioGc.length);
                            if (biblioGc != null) {

                                for (BiblioNotes note : biblioGc) {
                                    if (!note.getUserId().equals(userId) && !root.equals(userId)) {

                                        // se l'utente non è l'autore della nota
                                        gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                        break a;
                                    } else {
                                        idToDel.add(note.getId());  // se l'utente è l'autore della nota aggiungo l'id della nota
                                        // alla lista degli id delle note da eliminare
                                    }  // se l'utente è l'autore della nota aggiungo l'id della nota
                                    // alla lista degli id delle note da eliminare

                                }
                            }
                            int k = 0;
                            while (k < idToDel.size()) {
                                BiblioNotes note = vogc.selectBiblioNotes(Integer.parseInt(idToDel.get(k).toString()));
                                //  if(note.getType() == 1)

                                vogc.deleteBiblioNotes(note.getId());
                                k++;
                            }
                            // se l'utente è la fonte del valore dell'attributo e
                            // l'autore di tutte le sue note

                            vogc.deleteGclusterHasAttribute(vobjHasAttId);
                            gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED));
                        }
                        break;
                    case 2:
                        b:
                        {
                            PulsarHasAttribute plsHasAttObject = vogc.selectPulsarHasAttribute(vobjHasAttId);
                            if (!plsHasAttObject.getSourceId().equals(userId) && !root.equals(userId)) {

                                gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                break b;
                            }
                            biblioGc = vogc.selectBiblioNotesByGcAttributeId(vobjHasAttId);
                            idToDel = new ArrayList<>(biblioGc.length);
                            for (BiblioNotes note : biblioGc) {

                                if (!note.getUserId().equals(userId) && !root.equals(userId)) {
                                    gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                    break b;
                                } else {
                                    idToDel.add(note.getId());  // se l'utente è l'autore della nota aggiungo l'id della nota
                                    // alla lista degli id delle note da eliminare
                                }  // se l'utente è l'autore della nota aggiungo l'id della nota
                                // alla lista degli id delle note da eliminare
                            }
                            int k = 0;
                            while (k < idToDel.size()) {
                                BiblioNotes note = vogc.selectBiblioNotes(Integer.parseInt(idToDel.get(k).toString()));
                                if (note.getType() == 1) {
                                    vogc.deleteBiblioNotes(note.getId());
                                }
                                k++;
                            }
                            // se l'utente è la fonte del valore dell'attributo e
                            // l'autore di tutte le sue note
                            vogc.deletePulsarHasAttribute(vobjHasAttId);
                            gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED));
                        }
                        break;
                    case 3:
                        c:
                        {
                            StarHasAttribute starHasAttObject = vogc.selectStarHasAttribute(vobjHasAttId);
                            if (!starHasAttObject.getSourceId().equals(userId) && !root.equals(userId)) {
                                gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                break c;
                            }
                            biblioGc = vogc.selectBiblioNotesByGcAttributeId(vobjHasAttId);
                            idToDel = new ArrayList<>(biblioGc.length);
                            for (BiblioNotes note : biblioGc) {
                                if (!note.getUserId().equals(userId) && !root.equals(userId)) {
// se l'utente non è l'autore della nota
                                    gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                    break c;
                                } else {
                                    idToDel.add(note.getId());  // se l'utente è l'autore della nota aggiungo l'id della nota
                                    // alla lista degli id delle note da eliminare
                                }  // se l'utente è l'autore della nota aggiungo l'id della nota
                                // alla lista degli id delle note da eliminare
                            }
                            int k = 0;
                            while (k < idToDel.size()) {
                                BiblioNotes note = vogc.selectBiblioNotes(Integer.parseInt(idToDel.get(k).toString()));
                                if (note.getType() == 1) {
                                    vogc.deleteBiblioNotes(note.getId());
                                }
                                k++;
                            }
                            // se l'utente è la fonte del valore dell'attributo e
                            // l'autore di tutte le sue note
                            vogc.deleteStarHasAttribute(vobjHasAttId);
                            gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED));
                        }
                        break;

                }
            }
        } catch (TemplateException | IOException | VogcException | NumberFormatException ex) {
            Messages.sendError(ex, out);
        } finally {
            out.close();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
