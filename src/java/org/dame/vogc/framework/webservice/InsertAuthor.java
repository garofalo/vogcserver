package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.datatypes.Author;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author luca
 */
public class InsertAuthor extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out;
        response.setContentType("text/xml");
        out = response.getWriter();
        XMLGenerator gen = new XMLGenerator(out);

        String author = request.getParameter("Author");
        String uri = request.getParameter("URI");

        try {
            if (author == null) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            } else {

                IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
                Author newAuthor = new Author(0, author, uri);
                vogc.insertAuthor(newAuthor);
            }

        } catch (TemplateException | VogcException ex) {
            Logger.getLogger(InsertAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(InsertAuthor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(InsertAuthor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
