package org.dame.vogc.framework.webservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luca
 */
public class WebConfig {

    private static final String CONFIG_FILE = "c:\\vogcluster\\config\\FEConf.xml";
    private static String vogcUser_;
    private static String vogcPass_;

    static {

        File file = new File(CONFIG_FILE);
        try {

            BufferedReader r = new BufferedReader(new FileReader(file));
            vogcUser_ = r.readLine().trim();
            vogcPass_ = r.readLine().trim();

        } catch (IOException ex) {
            Logger.getLogger(WebConfig.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public static String getVogcUser() {
        return vogcUser_;
    }

    public static String getVogcPass() {
        return vogcPass_;
    }
}
