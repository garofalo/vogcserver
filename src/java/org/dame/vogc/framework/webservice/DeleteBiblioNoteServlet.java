package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.datatypes.BiblioNotes;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author Luca
 */
public class DeleteBiblioNoteServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userId = request.getParameter("userId");
        int biblioNoteId = Integer.parseInt(request.getParameter("noteId"));

        XMLGenerator gen;
        response.setContentType("text/xml");
        PrintWriter out = response.getWriter();
        gen = new XMLGenerator(out);
        try {
            if (userId == null || biblioNoteId < 0) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            } else {
                IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
                BiblioNotes biblio = vogc.selectBiblioNotes(biblioNoteId);
                if (!biblio.getUserId().equalsIgnoreCase(userId)) {
                    gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                } else {
                    vogc.deleteBiblioNotes(biblioNoteId);
                    gen.generateErrorReport(Messages.getError(Messages.NOTES_UPDATED));
                }
            }
        } catch (TemplateException | IOException | VogcException ex) {
            Messages.sendError(ex, out);
        } finally {
            out.close();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
