/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dame.vogc.framework.webservice;

import java.util.Properties;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Luca
 */
public class EMail {

    private static final String SMTP_HOST_NAME = "out.alice.it";
    private static final String SMTP_AUTH_USER = "sabrina_19";
    private static final String SMTP_AUTH_PWD = "06081908";
    private final String recipients[];
    private final String subject;
    private final String message;
    private final String from;

    public EMail(String rec[], String sub, String mes, String frm) {
        this.recipients = rec;
        this.subject = sub;
        this.message = mes;
        this.from = frm;
    }

    public void postMail() {
        //   try {
//        boolean debug = false;
        //Set the host smtp address
        Properties props = new Properties();
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        Authenticator auth = new SMTPAuthenticator();
        Session session = Session.getDefaultInstance(props, auth);
        // props.put("mail.smtp.auth", "true");

        Message msg = new MimeMessage(session);

        //   Authenticator auth = new SMTPAuthenticator();
        //   Session session = Session.getInstance(props, auth);
        //       session.setDebug(debug);
        // create a message
        //      Message msg = new MimeMessage(session);
        // set the from and to address
        /*
         * InternetAddress addressFrom = new InternetAddress(from);
         * msg.setFrom(addressFrom);
         * InternetAddress[] addressTo = new InternetAddress[recipients.length];
         * for (int i = 0; i < recipients.length; i++) {
         * addressTo[i] = new InternetAddress(recipients[i]);
         * }
         * msg.setRecipients(Message.RecipientType.TO, addressTo);
         * // Setting the Subject and Content Type
         * msg.setSubject(subject);
         * msg.setContent(message, "text/plain");
         * Transport.send(msg);
         * } catch (NamingException ex) {
         * Logger.getLogger(EMail.class.getName()).log(Level.SEVERE, null, ex);
         * } catch (MessagingException ex) {
         * Logger.getLogger(EMail.class.getName()).log(Level.SEVERE, null, ex);
         *
         * }
         */
        try {
            Address sender = new InternetAddress(from);
            Address recipient = new InternetAddress("HARRIS2010");

            msg.setFrom(sender);
            msg.setRecipient(Message.RecipientType.TO, recipient);
            msg.setSubject(subject);
            msg.setText("bla bla");

            Transport.send(msg);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private class SMTPAuthenticator extends Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            String username = SMTP_AUTH_USER;
            String password = SMTP_AUTH_PWD;
            return new PasswordAuthentication(username, password);
        }
    }
}
