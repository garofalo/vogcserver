package org.dame.vogc.framework.webservice;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.datatypes.VObject;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 * This servlet is invoked to retrieve all the informations about a Vobject
 *
 * @author Luca
 */
public class VObjectServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        XMLGenerator gen;
        PrintWriter out = response.getWriter();
        response.setContentType("text/xml");
        out = response.getWriter();
        gen = new XMLGenerator(out);
        String vobjectId = request.getParameter("objectId");
        //String vobjectId = "NGC 104";
        int type;
        try {
            if (vobjectId == null) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            } else {
                IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
                VObject obj = vogc.selectVObject(vobjectId);
                type = obj.getType();
                switch (type) {
                    case 1:
                        gen.createGclusterInfoList(vobjectId);
                        break;

                    case 2:
                        gen.createPulsarInfoList(vobjectId);
                        break;

                    case 3:
                        gen.createStarInfoList(vobjectId);
                        break;

                }
            }

        } catch (Exception ex) {
            Messages.sendError(ex, out);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
