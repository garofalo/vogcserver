package org.dame.vogc.framework.webservice;

import org.dame.vogc.datatypes.GcAttribute;
import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.datatypes.PlsAttribute;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.datatypes.StarAttribute;
import org.dame.vogc.datatypes.StarHasAttribute;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class NewAttributeServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/xml");
        out = response.getWriter();
        XMLGenerator gen = new XMLGenerator(out);

        String userId = request.getParameter("userId");
        String objectId = request.getParameter("objectId");
        String attributeName = request.getParameter("attName");
        String attributeDesc = request.getParameter("attDesc");
        String attributeUcd = request.getParameter("attUcd");
        String attributeDatatype = request.getParameter("attDatatype");
        String attributeValue = request.getParameter("attValue");
        int attributeType = Integer.parseInt(request.getParameter("type"));

        try {
            if (userId == null || objectId == null || attributeName == null || attributeDatatype == null || attributeValue == null) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            }

            IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
            int vobjectType = vogc.selectVObjectType(objectId);

            switch (vobjectType) {
                case 1: {
                    if (attributeType < 0 || attributeType > 5) {
                        gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
                    }
                    GcAttribute newAtt = new GcAttribute(0, attributeName, attributeDesc, attributeUcd, attributeDatatype, false, attributeType);
                    int newId = vogc.insertGcAttribute(newAtt);
                    GclusterHasAttribute gcHasAtt = new GclusterHasAttribute(0, objectId, newId, userId, attributeValue, true, null);
                    vogc.insertGclusterHasAttribute(gcHasAtt);
                    gen.generateErrorReport(Messages.getError(Messages.NEW_ATTRIBUTE_INSERTED));
                    break;
                }
                case 2: {
                    PlsAttribute newAtt = new PlsAttribute(0, attributeName, attributeDesc, attributeUcd, attributeDatatype, false);
                    int newId = vogc.insertPlsAttribute(newAtt);
                    PulsarHasAttribute plsHasAtt = new PulsarHasAttribute(0, newId, objectId, userId, attributeValue, null, true);
                    vogc.insertPulsarHasAttribute(plsHasAtt);
                    gen.generateErrorReport(Messages.getError(Messages.NEW_ATTRIBUTE_INSERTED));
                    break;
                }
                case 3: {
                    StarAttribute newAtt = new StarAttribute(0, attributeName, attributeDesc, attributeUcd, attributeDatatype, false);
                    int newId = vogc.insertStarAttribute(newAtt);
                    StarHasAttribute starHasAtt = new StarHasAttribute(0, newId, objectId, userId, attributeValue, null, true);
                    vogc.insertStarHasAttribute(starHasAtt);
                    gen.generateErrorReport(Messages.getError(Messages.NEW_ATTRIBUTE_INSERTED));
                    break;
                }
            }
        } catch (TemplateException | IOException | VogcException | SQLException ex) {
            Messages.sendError(ex, out);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
