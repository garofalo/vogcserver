package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author Luca
 */
public class UpdateAttributeValueServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/xml");
        out = response.getWriter();
        XMLGenerator gen = new XMLGenerator(out);
        String userId = request.getParameter("userId");
        int attObjectType = Integer.parseInt(request.getParameter("attObjectType")); // 1cluster,2pulsar,3star
        int attributeId = Integer.parseInt(request.getParameter("objectHasAttId")); //gcHasAtt, plsHasAtt or starHasAtt tuples id
        String newValue = request.getParameter("value");
        String objectName = request.getParameter("name");
        String attname = request.getParameter("attribute");

        try {
            if (userId == null || attributeId < 0 || newValue == null || attObjectType < 0 || attObjectType > 3) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            }

            IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
            // IVOGCAccess vogc2 = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);

            switch (attObjectType) {
//                case 1:
//                     GclusterHasAttribute gcHasAtt = vogc.selectGclusterHasAttribute(attributeId);
//                     if(gcHasAtt.getSourceId().compareTo(userId) == 0){
//                        vogc.updateGclusterHasAttValue(attributeId, newValue);
//                        vogc.updateGclusterHasAttDate(attributeId);
//                        gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_UPDATED));
//                     }
//                     else
//                         gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
//                     break;

                case 1:

//                    GclusterHasAttribute gcHasAtt2 = vogc.selectGclusterSourceId(objectName);
                    // if(gcHasAtt2.getSourceId().equals(userId)|| root.equals(userId)){
                    vogc.updateGclusterHasAttValue(newValue, objectName, attname);
                    vogc.updateGclusterHasAttDate(newValue, objectName, attname);
                    gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_UPDATED));
                    //    }
                    //  else
                    //      gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));

                    break;

                case 2:
//                    PulsarHasAttribute plsHasAtt = vogc.selectPulsarSourceId(objectName);
                    //   if(plsHasAtt.getSourceId().equals(userId)|| root.equals(userId)){
                    vogc.updatePulsarHasAttValue(newValue, objectName, attname);
                    vogc.updatePulsarHasAttDate(newValue, objectName, attname);
                    gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_UPDATED));
//                     }
//                     else
//                         gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                    break;

                case 3:
//                    StarHasAttribute starHasAtt = vogc.selectStarSourceId(objectName);

                    //      if (starHasAtt.getSourceId().equals(userId)|| root.equals(userId)){
                    vogc.updateStarHasAttValue(newValue, objectName, attname);
                    vogc.updateStarHasAttDate(newValue, objectName, attname);
                    gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_UPDATED));
                    // }
                    //else
                    //    gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                    break;
            }

        } catch (TemplateException | IOException | VogcException ex) {
            Messages.sendError(ex, out);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
