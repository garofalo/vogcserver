package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.datatypes.BiblioNotes;
import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.datatypes.StarHasAttribute;
import org.dame.vogc.datatypes.VObject;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author Luca
 */  // ELIMINAZIONE ATTRIBUTO
public class DeleteAttributeServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userId = request.getParameter("userId");
        String objectId = request.getParameter("objectId");
        String attName = request.getParameter("attName");
        int attId;
        String root = "admin";
        XMLGenerator gen;
        response.setContentType("text/xml");
        PrintWriter out = response.getWriter();
        gen = new XMLGenerator(out);
        int type;

        try {
            if (userId == null || objectId == null || attName == null) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            } else {
                IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
                VObject obj = vogc.selectVObject(objectId);
                type = obj.getType();
                BiblioNotes[] biblioGc;

                switch (type) {

                    case 1:
                        // globular cluster attribute value
                        a:
                        {
                            attId = vogc.selectGcAttributeIdByName(attName);  // seleziono l'id dell'attributo da eliminare
                            GclusterHasAttribute[] gcValues = vogc.selectGcHasAttByAttributeId(attId);  // seleziono tutti i valori presenti per l'attributo
                            for (GclusterHasAttribute val : gcValues) { // per ogni valore selezionato...
                                //if(!val.getSourceId().equals(userId) ){  // se l'utente non è la fonte del valore dell'attributo selezionato
                                if (!val.getSourceId().equals(userId) && !root.equals(userId)) {
                                    gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                    break a;
                                } else {
                                    biblioGc = vogc.selectBiblioNotesByGcAttributeId(val.getId());  // seleziono tutte le note associate a quel valore
                                    if (biblioGc != null) {
                                        for (BiblioNotes bn : biblioGc) {
                                            if (!bn.getUserId().equals(userId)) {  // se l'utente non è l'autore della nota
                                                gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                                break a;
                                            }
                                        }
                                    }
                                }

                            }
                            // se l'utente è la fonte del valore dell'attributo e l'autore di tutte le sue note
                            vogc.deleteGcAttribute(attId);  // posso procedere con l'eliminazione dell'attributo
                            if (vogc.selectGcHasAttByGclusterId(objectId).length == 1) {
                                gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED), "LAST_ATTRIBUTE");
                            } else {
                                gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED));
                            }
                        }
                        break;
                    case 2:
                        // pulsar attribute value
                        b:
                        {
                            attId = vogc.selectPlsAttributeIdByName(attName);  // seleziono l'id dell'attributo da eliminare
                            PulsarHasAttribute[] plsValues = vogc.selectPlsHasAttByAttributeId(attId);  // seleziono tutti i valori presenti per l'attributo
                            for (PulsarHasAttribute val : plsValues) { // per ogni valore selezionato...
                                //if(!val.getSourceId().equals(userId) ){  // se l'utente non è la fonte del valore dell'attributo selezionato
                                if (!val.getSourceId().equals(userId) && !root.equals(userId)) {
                                    gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                    break b;
                                } else {
                                    biblioGc = vogc.selectBiblioNotesByPlsAttributeId(val.getId());  // seleziono tutte le note associate a quel valore
                                    if (biblioGc != null) {
                                        for (BiblioNotes bn : biblioGc) {
                                            if (!bn.getUserId().equals(userId)) {  // se l'utente non è l'autore della nota
                                                gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                                break b;
                                            }
                                        }
                                    }
                                }
                            }
                            // se l'utente è la fonte del valore dell'attributo e l'autore di tutte le sue note
                            vogc.deletePlsAttribute(attId);  // posso procedere con l'eliminazione dell'attributo
                            if (vogc.selectPlsHasAttByPulsarId(objectId).length == 1) {
                                gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED), "LAST_ATTRIBUTE");
                            } else {
                                gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED));
                            }
                        }
                        break;
                    case 3:
                        // star attribute value
                        c:
                        {
                            attId = vogc.selectStarAttributeIdByName(attName);  // seleziono l'id dell'attributo da eliminare
                            StarHasAttribute[] starValues = vogc.selectStarHasAttByAttributeId(attId);  // seleziono tutti i valori presenti per l'attributo
                            for (StarHasAttribute val : starValues) { // per ogni valore selezionato...
                                //if(!val.getSourceId().equals(userId)){  // se l'utente non è la fonte del valore dell'attributo selezionato
                                if (!val.getSourceId().equals(userId) && !root.equals(userId)) {
                                    gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                    break c;
                                } else {
                                    biblioGc = vogc.selectBiblioNotesByStarAttributeId(val.getId());  // seleziono tutte le note associate a quel valore
                                    if (biblioGc != null) {
                                        for (BiblioNotes bn : biblioGc) {
                                            if (!bn.getUserId().equals(userId)) {  // se l'utente non è l'autore della nota
                                                gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                                                break c;
                                            }
                                        }
                                    }
                                }
                            }
                            // se l'utente è la fonte del valore dell'attributo e l'autore di tutte le sue note
                            vogc.deleteStarAttribute(attId);  // posso procedere con l'eliminazione dell'attributo
                            if (vogc.selectStarHasAttByStarId(objectId).length == 1) {
                                gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED), "LAST_ATTRIBUTE");
                            } else {
                                gen.generateErrorReport(Messages.getError(Messages.ATTRIBUTE_VALUE_DELETED));
                            }
                        }
                        break;
                }
            }

        } catch (TemplateException | IOException | VogcException ex) {
            Messages.sendError(ex, out);
        } finally {
            out.close();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
