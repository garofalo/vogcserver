package org.dame.vogc.framework.webservice;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.util.xml.XMLParser;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author Luca
 */
public class NewBiblioNoteResponseServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/xml");
        out = response.getWriter();
        int typeNote = Integer.parseInt(request.getParameter("type"));
        //      int typeNote = 1;
        SAXBuilder parser = new SAXBuilder();
        XMLGenerator xmlg = new XMLGenerator(out);
        XMLParser xmlp = new XMLParser();
        try {
            Document doc = parser.build(request.getInputStream());
            //      Document  doc = parser.build("C:/Users/Sabrina/Desktop/Templates/notesProva.xml");
            //  Document  doc = parser.build("C:/Documents and Settings/EM/Documenti/VOGCLUSTERS/VOGCLUSTERS/Freemarker Template/Templates/biblioRefProva.xml");
            switch (typeNote) {
                case 1: {
                    xmlp.parseNote(doc, out);
                    xmlg.generateErrorReport(Messages.getError(Messages.NOTES_INSERTED));
                    break;
                }
                case 2: {
                    xmlp.parseBiblioRef(doc, out);
                    xmlg.generateErrorReport(Messages.getError(Messages.NOTES_INSERTED));
                    break;
                }
            }

        } catch (Exception ex) {
            Messages.sendError(ex, out);

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
