package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.datatypes.Image;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author luca
 */
public class InsertNewImage extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, TemplateException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out;
        response.setContentType("text/xml");
        out = response.getWriter();
        XMLGenerator gen = new XMLGenerator(out);

        //int id  = Integer.parseInt(request.getParameter("id"));
        int id = 0;
        String objectId = request.getParameter("objectId");
        String userId = request.getParameter("userId");
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String format = request.getParameter("format");
        String dimension = request.getParameter("dimension");
        String uri = request.getParameter("uri");
        Date date;
        String scale = request.getParameter("scale");
        // int type  = Integer.parseInt(request.getParameter("type"));
        int type = 0;

//InsertNewImage?objectId=AM4&userId=admin2&name=terzo&description=ciccciobomba322&format=jpg&dimension=800x600&uri=www.www&scale=1:100
        Calendar cal = Calendar.getInstance();
        date = new Date(cal.getTimeInMillis());
        Image img = new Image(id, objectId, userId, name, description, format, dimension, uri, date, scale, type);
        try {
            if (img == null) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            } else {
                IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
                vogc.insertImage(img);
                gen.generateErrorReport(Messages.getError(Messages.NEW_ATTRIBUTE_INSERTED));
            }

        } catch (VogcException ex) {
            gen.generateErrorReport(Messages.getError(Messages.USERID_NULL));
            Logger.getLogger(InsertNewImage.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(InsertAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TemplateException ex) {
            Logger.getLogger(InsertNewImage.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(InsertAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TemplateException ex) {
            Logger.getLogger(InsertNewImage.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
