package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.datatypes.User;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author Luca
 */
public class AuthenticateServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        IVOGCAccess vogc = new VogcAccess("framework", "mfiore");
        String userId = request.getParameter("userId");
        String password = request.getParameter("password");

        //String userId = "admin";
        String pwd = null;
        try {
            pwd = vogc.selectUserPassword(userId);
        } catch (VogcException ex) {
            Logger.getLogger(AuthenticateServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        //String userId = null;
        PrintWriter out;
        XMLGenerator gen;
        response.setContentType("text/xml");
        out = response.getWriter();
        gen = new XMLGenerator(out);

        try {
            //Query string checks
            if (userId == null) {
                gen.generateErrorReport(Messages.getError(Messages.USERID_NULL));
            } else {

                User usr = vogc.selectUser(userId);

                if (usr.isActive()) {
                    //vogc.insertSession(userId);
                    if (usr.getName().equalsIgnoreCase("admin")) {
                        gen.generateErrorReport(Messages.getError(Messages.USER_ADMIN_ENABLED));
                    } else {
                        if (pwd.equals(password)) {
                            gen.generateErrorReport(Messages.getError(Messages.USER_ENABLED));
                        } else {
                            gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                        }
                    }

                } else {
                    gen.generateErrorReport(Messages.getError(Messages.USER_NOT_ENABLED));
                }

            }
        } catch (TemplateException | IOException | VogcException e) {
            Messages.sendError(e, out);
        } finally {
            out.close();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Authentication Servlet. Allows GET";
    }// </editor-fold>
}
