package org.dame.vogc.framework.webservice;

import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.datatypes.Source;
import org.dame.vogc.datatypes.User;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.framework.ServerConstants;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.access.IVOGCAccess;
import org.dame.vogc.access.VogcAccess;

/**
 *
 * @author luca
 */
public class InsertUserServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out;
        response.setContentType("text/xml");
        out = response.getWriter();
        XMLGenerator gen = new XMLGenerator(out);

        String userId = request.getParameter("userId");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String affiliation = request.getParameter("affiliation");
        String country = request.getParameter("country");
        boolean active = false;
        try {
            if (userId == null) {
                gen.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            }
            IVOGCAccess vogc = new VogcAccess(ServerConstants.DBUSER, ServerConstants.DBPWD);
            Source newSource = new Source(userId, 2);
            User newUser = new User(userId, password, name, surname, affiliation, country, active);
            try {
                User temp = vogc.selectUser(userId);
                if (temp.getId().equals(userId)) {
                    gen.generateErrorReport(Messages.getError(Messages.PERMISSION_DENIED));
                }
            } catch (VogcException ex) {
                try {
                    // String directory = userId;
                    //  boolean success = (new File("/home/luca/"+directory)).mkdir();
                    gen.generateErrorReport(Messages.getError(Messages.USER_ADDED));
                    vogc.insertSource(newSource, 1);
                    vogc.insertUser(newUser);
                    String directory = userId;
                    String DirFile = ServerConstants.DIR;
                    new File(DirFile + directory).mkdir();
                } catch (VogcException ex1) {
                    Logger.getLogger(InsertUserServlet.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } catch (TemplateException ex) {

            Logger.getLogger(InsertUserServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
