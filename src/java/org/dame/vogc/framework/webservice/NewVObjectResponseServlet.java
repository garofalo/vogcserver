package org.dame.vogc.framework.webservice;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;
import org.dame.vogc.util.xml.XMLParser;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

/**
 * This servlet is invoked to process the response sent by the client containing
 * information about a new globular cluster; the response is a XML document
 *
 * @author Luca
 */
public class NewVObjectResponseServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {

        PrintWriter out = response.getWriter();
        response.setContentType("text/xml");
        out = response.getWriter();
        SAXBuilder parser = new SAXBuilder();
        XMLParser xmlp = new XMLParser();
        XMLGenerator xmlg = new XMLGenerator(out);
        int vobjectType = Integer.parseInt(request.getParameter("vobjectType"));
        // 1 cluster, 2 pulsar, 3 star
        //      int vobjectType = 1;
        try {

            //     Document doc = parser.build("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.xml");
            //       Document  doc = parser.build("C:/Users/Sabrina/Desktop/Templates/clusterProva2.xml");
            //     Document  doc = parser.build("C:/Users/Sabrina/Desktop/Templates/clusterProva.xml");
            //    Document  doc = parser.build("C:/Users/Sabrina/Desktop/Templates/pulsarProva.xml");
            switch (vobjectType) {
                case 1: {
                    Document doc = parser.build(request.getInputStream());
                    xmlp.parseGclusterAttribute(doc, out);
                    xmlg.generateErrorReport(Messages.getError(Messages.VOBJECT_INSERTED));
                    break;
                }
                case 2: {
                    Document doc = parser.build(request.getInputStream());
                    xmlp.parsePulsarAttribute(doc, out);
                    xmlg.generateErrorReport(Messages.getError(Messages.VOBJECT_INSERTED));
                    break;
                }
                case 3: {
                    Document doc = parser.build(request.getInputStream());
                    xmlp.parseStarAttribute(doc, out);
                    xmlg.generateErrorReport(Messages.getError(Messages.VOBJECT_INSERTED));
                    break;
                }
                default:
                    xmlg.generateErrorReport(Messages.getError(Messages.PARAMETERS_NULL));
            }

        } catch (Exception ex) {
            Messages.sendError(ex, out);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(NewVObjectResponseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(NewVObjectResponseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
