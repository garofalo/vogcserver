package org.dame.vogc.framework.webservice;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dame.vogc.framework.exceptions.Messages;
import org.dame.vogc.util.xml.XMLGenerator;

/**
 *
 * @author Luca
 */
public class ClustersListServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * <p/>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out;
        XMLGenerator gen;

        response.setContentType("text/xml");
        out = response.getWriter();
        gen = new XMLGenerator(out);
        try {
            gen.createClustersList();

        } catch (Exception e) {
            Messages.sendError(e, out);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
