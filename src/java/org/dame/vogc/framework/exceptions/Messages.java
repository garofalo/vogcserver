package org.dame.vogc.framework.exceptions;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import org.dame.vogc.error.Error;
import org.dame.vogc.exceptions.VogcException;
import org.dame.vogc.util.xml.XMLGenerator;

/**
 *
 * @author luca
 */
public class Messages {

//    public final static String BADSSID = "BADSSID";
    public final static String USERID_NULL = "USERID_NULL";
    public final static String USER_ADDED = "USERID_ADDED";
    public final static String USER_ENABLED = "USER_ENABLED";
    public final static String USER_ADMIN_ENABLED = "USER_ADMIN_ENABLED";
    public final static String USER_NOT_ENABLED = "USER_NOT_ENABLED";
    public final static String PARAMETERS_NULL = "PARAMETERS_NULL";
    public final static String EMAIL_SENT = "EMAIL_SENT";
    public final static String PERMISSION_DENIED = "PERMISSION_DENIED";
    public final static String ATTRIBUTE_VALUE_DELETED = "ATTRIBUTE_VALUE_DELETED";
    public final static String NO_MATCH_FOUND = "NO_MATCH_FOUND";
    public final static String VOBJECT_INSERTED = "VOBJECT_INSERTED";
    public final static String VOBJECT_DELETED = "VOBJECT_DELETED";
    public final static String NOTES_INSERTED = "NOTES_INSERTED";
    public final static String NOTES_UPDATED = "NOTES_UPDATED";
    public final static String NEW_ATTRIBUTE_INSERTED = "NEW_ATTRIBUTE_INSERTED";
    public final static String ATTRIBUTE_UPDATED = "ATTRIBUTE_UPDATED";
    public final static String ATTRIBUTE_VALUE_UPDATED = "ATTRIBUTE_VALUE_UPDATED";
//    private final static String BADSSIDCODE = "ba01000000009";
    private final static String USERID_NULL_CODE = "0000";
    private final static String USER_ADDED_CODE = "vogcSRV0000";
    private final static String USER_ENABLED_CODE = "vogcSRV0001";
    private final static String USER_ADMIN_ENABLED_CODE = "vogcSRV0002";
    private final static String USER_NOT_ENABLED_CODE = "vogcSRV0003";
    private final static String PARAMETERS_NULL_CODE = "vogcSRV0004";
    private final static String EMAIL_SENT_CODE = "vogcSRV0005";
    private final static String PERMISSION_DENIED_CODE = "vogcSRV0006";
    private final static String ATTRIBUTE_VALUE_DELETED_CODE = "vogcSRV0007";
    private final static String NO_MATCH_FOUND_CODE = "vogcSRV0008";
    private final static String VOBJECT_INSERTED_CODE = "vogcSRV009";
    private final static String NOTES_INSERTED_CODE = "vogcSRV010";
    private final static String NEW_ATTRIBUTE_INSERTED_CODE = "vogcSRV011";
    private final static String ATTRIBUTE_UPDATED_CODE = "vogcSRV012";
    private final static String ATTRIBUTE_VALUE_UPDATED_CODE = "vogcSRV013";
    private final static String NOTES_UPDATED_CODE = "vogcSRV014";
    private final static String VOBJECT_DELETED_CODE = "vogcSRV015";
    private static final Map<String, Error> errorMap_;

    static {
        errorMap_ = new HashMap<>(20);

        errorMap_.put(USERID_NULL, new Error(USERID_NULL_CODE, "UserId is null"));
        errorMap_.put(USER_ADDED, new Error(USER_ADDED_CODE, "User Added"));
        errorMap_.put(USER_ENABLED, new Error(USER_ENABLED_CODE, "User DAME Enabled"));
        errorMap_.put(USER_ADMIN_ENABLED, new Error(USER_ADMIN_ENABLED_CODE, "User Admin DAME Enabled"));
        errorMap_.put(USER_NOT_ENABLED, new Error(USER_NOT_ENABLED_CODE, "User DAME Not Enabled"));
        errorMap_.put(PARAMETERS_NULL, new Error(PARAMETERS_NULL_CODE, "One or more parameters are wrong or null"));
        errorMap_.put(EMAIL_SENT, new Error(EMAIL_SENT_CODE, "Sucessfully sent mail to the administrator"));
        errorMap_.put(PERMISSION_DENIED, new Error(PERMISSION_DENIED_CODE, "User do not have the permission for this operation"));
        errorMap_.put(ATTRIBUTE_VALUE_DELETED, new Error(ATTRIBUTE_VALUE_DELETED_CODE, "Successfully attribute deleted"));
        errorMap_.put(NO_MATCH_FOUND, new Error(NO_MATCH_FOUND_CODE, "No Match Found"));
        errorMap_.put(VOBJECT_INSERTED, new Error(VOBJECT_INSERTED_CODE, "Successfully vobject inserted"));
        errorMap_.put(NOTES_INSERTED, new Error(NOTES_INSERTED_CODE, "Successfully notes inserted"));
        errorMap_.put(NEW_ATTRIBUTE_INSERTED, new Error(NEW_ATTRIBUTE_INSERTED_CODE, "Successfully attribute inserted"));
        errorMap_.put(ATTRIBUTE_UPDATED, new Error(ATTRIBUTE_UPDATED_CODE, "Successfully attribute information updated"));
        errorMap_.put(ATTRIBUTE_VALUE_UPDATED, new Error(ATTRIBUTE_VALUE_UPDATED_CODE, "Successfully attribute value updated"));
        errorMap_.put(NOTES_UPDATED, new Error(NOTES_UPDATED_CODE, "Successfully notes updated"));
        errorMap_.put(VOBJECT_DELETED_CODE, new Error(VOBJECT_DELETED_CODE, "Successfully vobject deleted"));

        //    errorMap_.put(NO_MATCH_FOUND, new Error(NO_MATCH_FOUND_CODE, "No Match Found"));

        /*
         * errorMap_.put(BADSSID,new Error(BADSSIDCODE,"Bad SSID"));
         * errorMap_.put(BADIP,new Error(BADIPCODE,"Bad SSID"));
         * errorMap_.put(OK,new Error(OKCODE,"Bad SSID"));
         * errorMap_.put(GENERICERROR,new Error(GENERICERRORCODE,"Bad SSID"));
         * errorMap_.put(FUNCTIONALITYNOTFOUND, new
         * Error(FUNCTIONALITYNOTFOUNDCODE,"Functionality not found"));
         * errorMap_.put(BADUPLOAD, new Error(BADUPLOADCODE,"Bad upload"));
         * errorMap_.put(WRONGSIZE, new Error(WRONGSIZECODE,"Wrong size"));
         * errorMap_.put(BADCONFIGFILE, new Error(BADCONFIGFILECODE,"Bad config
         * file"));
         * errorMap_.put(WRONGMETHOD, new Error(WRONGMETHODCODE,"Wrong
         * method"));
         * errorMap_.put(BADREQUEST, new Error(BADREQUESTCODE, "Bad Request"));
         * errorMap_.put(CONTINUE, new Error(CONTINUECODE, "The operation is not
         * over"));
         */
    }

    public static String getErrorString(String error) {
        Error e = errorMap_.get(error);
        return e.getCode() + "- " + e.getMessage();
    }

    public static Error getError(String error) {
        return errorMap_.get(error);
    }

    public static void sendError(Exception e, PrintWriter out) {
        if (e instanceof VogcException) {
            try {
                VogcException ex = (VogcException) e;
                XMLGenerator gen = new XMLGenerator(out);
                gen.generateErrorReport(ex.getError(), ex.getError().getMessage());

            } catch (IOException | TemplateException ex) {
                out.print(ex.getMessage());
            }
        } else {
            out.print(e.getMessage());

        }

    }
}
